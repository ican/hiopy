from hiopy.utils.filter_unwanted import filter_unwanted

def test_filter_unwanted_nothing():
    assert filter_unwanted({}) == {}
    assert filter_unwanted({"foo": "bar"}) == {"foo": "bar"}

def test_filter_unwanted_all_wanted():
    assert (filter_unwanted({
        "components": {
            "A": {
                "have": ["v1", "v2"],
            },
            "B": {
                "want": ["v1", "v2"],
            }
        }
    }) == {
        "components": {
            "A": {
                "have": ["v1", "v2"],
            },
            "B": {
                "want": ["v1", "v2"],
            }
        }
    })

def test_filter_unwanted_some_wanted():
    assert (filter_unwanted({
        "components": {
            "A": {
                "have": ["v1", "v2", "v3"],
            },
            "B": {
                "want": ["v1", "v2"],
            }
        }
    }) == {
        "components": {
            "A": {
                "have": ["v1", "v2"],
            },
            "B": {
                "want": ["v1", "v2"],
            }
        }
    })

def test_filter_unwanted_some_wanted_mixed():
    assert (filter_unwanted({
        "components": {
            "A": {
                "have": [{"v1": {"size": 2}}, "v3"],
                "want": [{"v2": {"time": {"lag": 1}}}],
            },
            "B": {
                "have": ["v2", "v4"],
                "want": ["v1"],
            }
        }
    }) == {
        "components": {
            "A": {
                "have": [{"v1": {"size": 2}}],
                "want": [{"v2": {"time": {"lag": 1}}}],
            },
            "B": {
                "have": ["v2"],
                "want": ["v1"],
            }
        }
    })


def test_filter_unwanted_some_wanted_keep():
    assert (filter_unwanted({
        "components": {
            "A": {
                "have": ["v1", "v2", "v3", "v4"],
            },
            "B": {
                "want": ["v1", "v2"],
            }
        }
    }, keep=["v3"]) == {
        "components": {
            "A": {
                "have": ["v1", "v2", "v3"],
            },
            "B": {
                "want": ["v1", "v2"],
            }
        }
    })
