from pathlib import Path
import os
import shutil
import subprocess
import pytest
from hiopy.utils.init_zarr import init_zarr
from hiopy.utils.extend_time import extend_time

import numpy as np
import numpy.testing as npt
import xarray as xr
import numcodecs
import fsspec

yac = pytest.importorskip("yac")

testdir = Path(__file__).parent.absolute()

default_encoding = {
    "compressor": {
        "blocksize": 0,
        "clevel": 5,
        "cname": "zstd",
        "id": "blosc",
        "shuffle": 1,
    },
    "dtype": "<f8",
    "fill_value": "NaN",
    "filters": None,
}

timevar_descr = {
    "dims": ["time"],
    "encoding": {
        "dtype": "<i4",
        "fill_value": None,
    },
    "attrs": {
        "units": "seconds since 1970-01-01",
        "calendar": "proleptic_gregorian",
    },
}

def run(cmdline, *args, **kwargs):
    print(args, kwargs)
    print(" ".join(map(str, cmdline)), flush=True)
    return subprocess.check_output(cmdline, *args, **kwargs, timeout=10)


def prepare_coupling_config(simple_coupling, rundir):
    # for YAC2
    run(["hiopy", "s2x",
         "-i", simple_coupling,
         "-o", rundir / "coupling.xml"])

    # for YAC3
    run(["hiopy", "s2c",
         "-i", simple_coupling,
         "-o", rundir / "ccoupling.yaml"])


@pytest.fixture
def rundir(tmpdir):
    shutil.copyfile(testdir / "coupling.xsd", tmpdir / "coupling.xsd")

    prepare_coupling_config(testdir / "testdata_coupling.yaml", tmpdir)

    return tmpdir


@pytest.fixture
def mlrundir(tmpdir):
    shutil.copyfile(testdir / "coupling.xsd", tmpdir / "coupling.xsd")

    prepare_coupling_config(testdir / "testdata_coupling_coarsening.yaml", tmpdir)

    return tmpdir


@pytest.fixture
def tsrundir(tmpdir):
    shutil.copyfile(testdir / "coupling.xsd", tmpdir / "coupling.xsd")

    prepare_coupling_config(testdir / "testdata_coupling_two_sinks.yaml", tmpdir)

    return tmpdir


def test_basic_yac_coupling(rundir):
    run(["mpirun", "-n", "1", "python3",
         str(testdir / "gen_yac_testdata.py"),
         ":", "-n", "1", "python3",
         str(testdir / "read_yac_testdata.py")],
        cwd=rundir)


def test_two_sink_yac_coupling(tsrundir):
    run(["mpirun", "-n", "1", "python3",
         str(testdir / "gen_yac_testdata.py"),
         ":", "-n", "1", "python3",
         str(testdir / "read_yac_testdata.py"),
         ":", "-n", "1", "python3",
         str(testdir / "read_yac_testdata.py"),
         "-c", "test_sink2",
         "-g", "sink_grid2"],
        cwd=tsrundir)


@pytest.mark.parametrize("dtype", ["<f4", "<f8"])
def test_yac_to_zarr_chunks(rundir, dtype):
    store = fsspec.get_mapper(str(rundir / "testout"))
    descr = {
        "dimensions": {"time": 0, "cell": 12},
        "chunks": [{"time": 11}, {"time": 1, "cell": 12}],
        "encoding": {**default_encoding, "dtype": dtype},
        "variables": {
            "time": timevar_descr,
            "test1": ["time", "cell"],
        }
    }
    init_zarr(descr, store=store)
    extend_time(store, "2020-01-01T00:00:00", "2020-01-01T00:00:10", "PT1S")

    run(["mpirun", "-n", "1", "python3",
         str(testdir / "gen_yac_testdata.py"),
         ":", "-n", "1",
         "hiopy", "healpix_output",
         "-b", "0", "-n", "1", "-i", "0", "-v", "test1",
         "-o", "testout", "-c", "test_sink", "-g", "sink_grid"],
        cwd=rundir)

    i = np.arange(11)[:, np.newaxis]
    refdata = (np.arange(12) + 1000 * i).astype(dtype)

    ds = xr.open_dataset(store, engine="zarr")
    assert np.all(ds.test1.values == refdata)


@pytest.mark.parametrize("output_thread", [False, True])
@pytest.mark.parametrize("dtype", ["<f4", "<f8"])
def test_yac_to_zarr_chunks_with_nan(rundir, dtype, output_thread):
    store = fsspec.get_mapper(str(rundir / "testout"))
    descr = {
        "dimensions": {"time": 0, "cell": 12},
        "chunks": [{"time": 11}, {"time": 1, "cell": 12}],
        "encoding": {**default_encoding, "dtype": dtype},
        "variables": {
            "time": timevar_descr,
            "test1": ["time", "cell"],
        }
    }
    init_zarr(descr, store=store)
    extend_time(store, "2020-01-01T00:00:00", "2020-01-01T00:00:10", "PT1S")

    run(["mpirun", "-n", "1", "python3",
         str(testdir / "gen_yac_testdata.py"), "--with_nan",
         ":", "-n", "1",
         "hiopy", "healpix_output",
         "-b", "0", "-n", "1", "-i", "0", "-v", "test1",
         "-o", "testout", "-c", "test_sink", "-g", "sink_grid"] + (["--output_in_thread"] if output_thread else []),
        cwd=rundir)

    i = np.arange(11)[:, np.newaxis]
    refdata = (np.concatenate([[np.nan] * 4, np.arange(4, 12)]) + 1000 * i).astype(dtype)

    ds = xr.open_dataset(store, engine="zarr")
    npt.assert_array_equal(ds.test1.values, refdata)


def test_yac_to_zarr_chunks_with_offset(rundir):
    store = fsspec.get_mapper(str(rundir / "testout"))
    descr = {
        "dimensions": {"time": 0, "cell": 12},
        "chunks": [{"time": 11}, {"time": 1, "cell": 12}],
        "encoding": default_encoding,
        "variables": {
            "time": timevar_descr,
            "test1": ["time", "cell"],
        }
    }
    init_zarr(descr, store=store)
    extend_time(store, "2019-12-31T23:59:50", "2020-01-01T00:00:10", "PT1S")

    run(["mpirun", "-n", "1", "python3",
         str(testdir / "gen_yac_testdata.py"),
         ":", "-n", "1",
         "hiopy", "healpix_output",
         "-b", "0", "-n", "1", "-i", "0", "-v", "test1",
         "-o", "testout", "-c", "test_sink", "-g", "sink_grid"],
        cwd=rundir)

    assert set(os.listdir(rundir / "testout" / "test1")) == {".zarray", ".zattrs"} | {str(i) for i in range(10, 21)}


def test_yac_to_zarr_chunks_multilevel(mlrundir):
    store1 = fsspec.get_mapper(str(mlrundir / "testout1"))
    store0 = fsspec.get_mapper(str(mlrundir / "testout0"))
    descr1 = {
        "dimensions": {"time": 0, "cell": 48},
        "chunks": [{"time": 11}, {"time": 1, "cell": 48}],
        "encoding": default_encoding,
        "variables": {
            "time": timevar_descr,
            "test1": ["time", "cell"],
        }
    }
    descr0 = {
        "dimensions": {"time": 0, "cell": 12},
        "chunks": [{"time": 11}, {"time": 1, "cell": 12}],
        "encoding": default_encoding,
        "variables": {
            "time": timevar_descr,
            "test1": ["time", "cell"],
        }
    }
    init_zarr(descr1, store=store1)
    init_zarr(descr0, store=store0)
    extend_time(store1, "2020-01-01T00:00:00", "2020-01-01T00:00:10", "PT1S")
    extend_time(store0, "2020-01-01T00:00:00", "2020-01-01T00:00:10", "PT1S")

    cmd = ["mpirun", "-n", "1", "python3",
           str(testdir / "gen_yac_testdata.py"),
           ":", "-n", "1",
           "hiopy", "healpix_output",
           "-n", "1", "-i", "0", "-v", "test1",
           "-o", "testout1", "-c", "test_sink", "-g", "sink_grid",
           "-b", "1", "-s", "1", "-l", "0",
           ":", "-n", "1",
           "hiopy", "healpix_output",
           "-n", "1", "-i", "0", "-v", "test1",
           "-b", "0", "-s", "0", "-l", "1",
           "-o", "testout0", "-c", "test_sink", "-g", "sink_grid",
           ]

    run(cmd, cwd=mlrundir)

    i = np.arange(11)[:, np.newaxis]
    refdata = (np.arange(12) + 1000 * i).astype("<f8")

    ds = xr.open_dataset(store0, engine="zarr")
    assert np.all(ds.test1.values == refdata)


def test_yac_to_zarr_meteogram(rundir):
    store = fsspec.get_mapper(str(rundir / "testout"))
    descr = {
        "dimensions": {"time": 0, "station": 2},
        "chunks": [{"time": 11}, {"time": 100, "station": 2}],
        "encoding": default_encoding,
        "variables": {
            "time": timevar_descr,
            "test1": ["time", "station"],
        }
    }
    init_zarr(descr, store=store)
    extend_time(store, "2020-01-01T00:00:00", "2020-01-01T00:00:10", "PT1S")

    testcmd = ["mpirun", "-n", "1", "python3",
               str(testdir / "gen_yac_testdata.py"),
               ":", "-n", "1",
               "hiopy", "meteogram_output",
               "--station_file", str(testdir / "testdata_meteogram_stations.yaml"),
               "-v", "test1", "-o", "testout", "-c", "test_sink", "-g", "sink_grid",
               ]
    run(testcmd, cwd=rundir)

    assert set(os.listdir(rundir / "testout" / "test1")) == {".zarray", ".zattrs", "0"}
    assert set(os.listdir(rundir / "testout" / "test1" / "0")) == {"0"}

    refdata = (1000 * np.arange(11, dtype="<f8")[:, np.newaxis] + np.array([3, 9])).astype("<f8")
    ds = xr.open_dataset(store, engine="zarr")
    assert np.all(ds.test1.values == refdata)
