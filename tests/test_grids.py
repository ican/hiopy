from hiopy.grids.base import lonlat2xyz, xyz2lonlat

import pytest
import numpy as np
import numpy.testing as npt

LLXYZ_TEST_CASES = [
    ([0, 0], [1, 0, 0]),
    ([90, 0], [0, 1, 0]),
    ([0, 90], [0, 0, 1]),
    ([180, 0], [-1, 0, 0]),
    ([-90, 0], [0, -1, 0]),
    ([0, -90], [0, 0, -1]),
    ([45, 0], [.5**.5, .5**.5, 0]),
    ([45, 45], [.5, .5, .5**.5]),
]

@pytest.mark.parametrize("lonlat,xyz", LLXYZ_TEST_CASES)
def test_assert_lonlat_xyz_conversion(lonlat, xyz):
    npt.assert_array_almost_equal(lonlat2xyz(*lonlat), xyz)
    npt.assert_array_almost_equal(xyz2lonlat(xyz), lonlat)
