import numpy as np
import packaging.version
import yac

from hiopy.grids.healpix import HealPixSubgridDefinition
from hiopy.yac_interface import make_yac_grid, parse_datetime, parse_duration

def run_yac2(args):
    from yac import YAC, Role, Field

    yac = YAC("coupling.xml", "coupling.xsd")
    comp_id = yac.def_comp("test_source")

    gridspec = HealPixSubgridDefinition(args.nside, 1, 0)

    print(f"grid has {gridspec.total_number_of_cells} cells in total")

    point_id, grid, npoints = make_yac_grid(gridspec, "source_grid")

    field1 = Field.create("test1", comp_id, point_id)
    field2 = Field.create("test2", comp_id, point_id)

    print("start search")

    yac.search()

    timestep = parse_duration(field1.model_timestep)
    start = parse_datetime(yac.start_datetime)
    end = parse_datetime(yac.end_datetime)
    steps = int((end - start) / timestep) + 1
    print(f"YAC period: {yac.start_datetime} ... {yac.end_datetime} in {steps} steps")

    for i in range(steps):
        data1 = np.arange(gridspec.cells_per_chunk * field1.collection_size, dtype="<f8") + 1000 * i
        if args.with_nan:
            data1[:len(data1) // 3] = np.nan
        data2 = np.arange(gridspec.cells_per_chunk * field2.collection_size, dtype="<f8") + 1000 * i + 100
        for field, data in [(field1, data1), (field2, data2)]:
            if field.role == Role.SOURCE:
                field.put(data)


def run_yac3(args):
    yac.def_calendar(yac.Calendar.PROLEPTIC_GREGORIAN)

    yac_instance = yac.YAC()
    yac_instance.def_datetime("2020-01-01T00:00:00.000", "2020-01-01T00:00:10.000")

    comp_id = yac_instance.def_comp("test_source")

    gridspec = HealPixSubgridDefinition(args.nside, 1, 0)

    print(f"grid has {gridspec.total_number_of_cells} cells in total")

    point_id, grid, npoints = make_yac_grid(gridspec, "source_grid")

    field1 = yac.Field.create("test1", comp_id, point_id, 1, "PT1S", yac.TimeUnit.ISO_FORMAT)
    field2 = yac.Field.create("test2", comp_id, point_id, 2, "PT1S", yac.TimeUnit.ISO_FORMAT)

    print("start search")

    yac_instance.enddef()

    timestep = parse_duration(field1.timestep)
    start = parse_datetime(yac_instance.start_datetime)
    end = parse_datetime(yac_instance.end_datetime)
    steps = int((end - start) / timestep) + 1
    print(f"YAC period: {yac_instance.start_datetime} ... {yac_instance.end_datetime} in {steps} steps")

    for i in range(steps):
        data1 = np.arange(gridspec.cells_per_chunk * field1.collection_size, dtype="<f8") + 1000 * i
        if args.with_nan:
            data1[:len(data1) // 3] = np.nan
        data2 = np.arange(gridspec.cells_per_chunk * field2.collection_size, dtype="<f8") + 1000 * i + 100
        for field, data in [(field1, data1), (field2, data2)]:
            if field.role == yac.ExchangeType.SOURCE:
                field.put(data)
    print("sender done")


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--nside", default=1, type=int, help="nside for source grid")
    parser.add_argument("--with_nan", default=False, action="store_true", help="add NaN data on northern row")
    args = parser.parse_args()

    yac_version = packaging.version.parse(yac.version())
    if yac_version.major == 2:
        return run_yac2(args)
    elif yac_version.major == 3:
        return run_yac3(args)


if __name__ == "__main__":
    main()
