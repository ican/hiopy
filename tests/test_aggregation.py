from hiopy.aggregation import SumAggregation, NanSumAggregation, MeanAggregation, NanMeanAggregation

import pytest

import numpy as np
import numpy.testing as npt

@pytest.mark.parametrize("Agg,reffunc", [(SumAggregation, np.sum),
                                         (NanSumAggregation, np.nansum),
                                         (MeanAggregation, np.mean),
                                         (NanMeanAggregation, np.nanmean)])
@pytest.mark.parametrize("buffer", [np.arange(16).astype("float"),
                                    [1., np.nan, 2., 3.,
                                     np.nan, np.nan, np.nan, np.nan,
                                     4., 5., 7., 8.,
                                     6., np.nan, 9., np.nan]])
def test_aggregation(Agg, reffunc, buffer):
    buffer = np.array(buffer)

    agg = Agg(4)

    b = agg.prepare(buffer)
    for i in range(2):
        b = agg.aggregate(b)
    res = agg.finalize(b)

    npt.assert_almost_equal(res, reffunc(buffer))
