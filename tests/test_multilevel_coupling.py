from hiopy.utils.multilevel_coupling import build_multilevel_coupling
from hiopy.aggregation import get_aggregation

import pytest

DEFAULT_INTERPOLATION = {
    "space": [{
        "method": "n-nearest_neighbor",
        "n": 1,
        "weighted": "ARITHMETIC_AVERAGE"
    }, {
        "method": "fixed_value",
        "user_value": -999.9,
    }],
    "time": "average"
}

MULTILEVEL_INTERPOLATION = {
    "space": [{
        "method": "n-nearest_neighbor",
        "n": 1,
        "weighted": "ARITHMETIC_AVERAGE"
    }],
    "time": "none"
}


@pytest.mark.parametrize("aggregation,have_extra", [("mean", {}), ("nanmean", {"size": 2})])
def test_multilevel_coupling(aggregation,have_extra):
    config = {
        "components": {
            "sim": {
                "model": "atm",
                "grid": "ico",
                "have": ["tas"],
            },
            "out": {
                "model": "output",
                "grid": "hp",
                "want": ["tas"],
            },
        },
        "interpolations": {
            "default": DEFAULT_INTERPOLATION,
        }
    }

    ref_config = {
        "components": {
            "sim": {
                "model": "atm",
                "grid": "ico",
                "have": ["tas"],
            },
            "out": {
                "model": "output",
                "grid": "hp",
                "have": {"tas_out_l1": {"grid": "hp_c__l1", **have_extra}},
                "want": {"tas": {}},
            },
            "out__l1": {
                "model": "output",
                "grid": "hp__l1",
                "have": {},
                "want": {"tas_out_l1": {"interpolation": "__hiopy_multilevel"}} ,
            }
        },
        "interpolations": {
            "default": DEFAULT_INTERPOLATION,
            "__hiopy_multilevel": MULTILEVEL_INTERPOLATION,
        }
    }

    mlconfig = build_multilevel_coupling(config, 1, get_aggregation(aggregation))
    assert mlconfig == ref_config


@pytest.mark.parametrize("wantlist", [{"tas": {"interpolation": "foo"}},
                                      [{"tas": {"interpolation": "foo"}}]])
def test_multilevel_coupling_with_interpolation(wantlist):
    config = {
        "components": {
            "sim": {
                "model": "atm",
                "grid": "ico",
                "have": ["tas"],
            },
            "out": {
                "model": "output",
                "grid": "hp",
                "want": wantlist,
            },
        },
        "interpolations": {
            "default": DEFAULT_INTERPOLATION,
            "__hiopy_multilevel": MULTILEVEL_INTERPOLATION,
        }
    }

    ref_config = {
        "components": {
            "sim": {
                "model": "atm",
                "grid": "ico",
                "have": ["tas"],
            },
            "out": {
                "model": "output",
                "grid": "hp",
                "have": {"tas_out_l1": {"grid": "hp_c__l1", "size": 2}},
                "want": {
                    "tas": {
                        "interpolation": "foo",
                    },
                },
            },
            "out__l1": {
                "model": "output",
                "grid": "hp__l1",
                "have": {},
                "want": {
                    "tas_out_l1": {
                        "interpolation": "__hiopy_multilevel",
                    },
                },
            }
        },
        "interpolations": {
            "default": DEFAULT_INTERPOLATION,
            "__hiopy_multilevel": MULTILEVEL_INTERPOLATION,
        }
    }

    mlconfig = build_multilevel_coupling(config, 1)
    assert mlconfig == ref_config
