import argparse
import numpy as np
import numpy.testing as npt

import logging

from hiopy.grids.healpix import HealPixSubgridDefinition
from hiopy.yac_interface import YACComponent


def main():
    logging.basicConfig(level="DEBUG")
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", default=False, action="store_true")
    parser.add_argument("-c", "--component", default="test_sink", type=str)
    parser.add_argument("-g", "--grid", default="sink_grid", type=str)
    args = parser.parse_args()

    gridspec = HealPixSubgridDefinition(1, 1, 0)
    component = YACComponent(args.component)
    listener = component.get_listener(args.grid, gridspec, ["test1", "test2"])

    field_offsets = {"test1": 0, "test2": 100}
    field_collection_size = {"test1": 1, "test2": 2}

    import sys
    print("activating", file=sys.stderr)
    component.activate()

    def f(itime):
        if args.component == "test_sink2":
            return 2000 * itime
        else:
            return 1000 * itime

    print("receiving", file=sys.stderr)
    for fieldname, itime, timestamp, buffer in listener:
        if args.verbose:
            print(fieldname, itime, timestamp)
            print(buffer)
        refdata = np.arange(field_collection_size[fieldname] * 12, dtype="<f8") + f(itime) + field_offsets[fieldname]
        refdata = refdata.reshape(-1, 12)
        npt.assert_equal(buffer, refdata)

    print("coupling works")

    return 0

if __name__ == "__main__":
    exit(main())
