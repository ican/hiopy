import datetime
import json
from queue import Queue
import threading

import numpy as np
import numpy.testing as npt
import zarr
from zarr.storage import MemoryStore
import numcodecs

from hiopy.utils.healpix_output import run_output
from hiopy.grids.healpix import HealPixSubgridDefinition

from hiopy.utils.init_zarr import init_zarr
from hiopy.utils.extend_time import extend_time

import pytest

def default_data_generator(gridspec, varname, i):
    return np.arange(gridspec.cells_per_chunk, dtype="<f8")[np.newaxis, :]


class DummyTransmitter:
    name = "dummy"

    def __init__(self, source=None, sink=None):
        self.source = source
        self.sink = sink

    def activate(self):
        pass

    def get_listener(self, gridname, gridspec, listen_varnames):
        return self.source.get_listener(gridname, gridspec, listen_varnames)

    def get_emitter(self, gridname, gridspec, fieldnames):
        return self.sink.get_emitter(gridname, gridspec, fieldnames)


class DummySource:
    def __init__(self, count=1, generator=None, time_offset=0):
        self.count = count
        self.reftime = datetime.datetime(2023, 1, 1)
        self.timestep = datetime.timedelta(seconds=1)
        self.reftime += time_offset * self.timestep
        self.generator = generator or default_data_generator

    def get_listener(self, gridname, gridspec, listen_varnames):
        for i in range(self.count):
            for varname in listen_varnames:
                yield varname, i, np.datetime64(self.reftime + i * self.timestep), self.generator(gridspec, varname, i)


class DummyGridspec:
    def coarsen(self, steps):
        return DummyGridspec()

    @property
    def cell_chunk_slice(self):
        return slice(None)


class FieldStoreEmitter:
    def __init__(self, store):
        self.store = store

    def emit(self, fieldname, itime, timestamp, buffer):
        self.store[fieldname, itime] = buffer

class FieldStoreSink:
    def __init__(self, store):
        self.store = store

    def get_emitter(self, gridname, gridspec, fieldnames):
        return FieldStoreEmitter(self.store)


class MessagePipeEmitter:
    def __init__(self, queue):
        self.queue = queue

    def emit(self, fieldname, itime, timestamp, buffer):
        self.queue.put((fieldname, itime, timestamp, buffer))


class MessagePipe:
    def __init__(self):
        self.queue = Queue()

    def get_listener(self, gridname, gridspec, listen_varnames):
        while True:
            res = self.queue.get()
            if res == "DONE":
                self.queue.task_done()
                return
            else:
                yield res
                self.queue.task_done()

    def get_emitter(self, gridname, gridspec, fieldnames):
        return MessagePipeEmitter(self.queue)

    def join(self):
        self.queue.put("DONE")
        return self.queue.join()


class MemoryStore2(MemoryStore):
    def setitems(self, items):
        for k, v in items.items():
            self[k] = v

default_encoding = {
    "compressor": {
        "blocksize": 0,
        "clevel": 5,
        "cname": "zstd",
        "id": "blosc",
        "shuffle": 1,
    },
    "dtype": "<f8",
    "fill_value": "NaN",
    "filters": None,
}

timevar_descr = {
    "dims": ["time"],
    "encoding": {
        "dtype": "<i4",
        "fill_value": None,
    },
    "attrs": {
        "units": "seconds since 1970-01-01",
        "calendar": "proleptic_gregorian",
    },
}

def zarray(varname, shape, chunks):
    return json.dumps({
        "chunks": list(chunks),
        "compressor": {"id": "blosc", "cname": "zstd", "clevel": 5, "shuffle": 1},
        "dtype": "<f8",
        "fill_value": "NaN",
        "filters": None,
        "order": "C",
        "shape": list(shape),
        "dimension_separator": "/",
        "zarr_format": 2,
    }).encode("utf-8")


def test_output():
    store = MemoryStore2()
    descr = {
        "dimensions": {"time": 0, "cell": 12},
        "chunks": [{"time": 1}, {"time": 1, "cell": 12}],
        "encoding": default_encoding,
        "variables": {
            "time": timevar_descr,
            "test": ["time", "cell"],
        }
    }
    init_zarr(descr, store=store)
    extend_time(store, "2023-01-01T00:00:00", "2023-01-01T00:00:00", "PT1S")

    run_output(component=DummyTransmitter(source=DummySource()),
               gridspec=HealPixSubgridDefinition(1, 1, 0),
               varnames=["test"],
               store=store)

    assert np.all(zarr.open_group(store)["test"][:] == np.arange(12, dtype="<f8")[np.newaxis, :])


def test_output_with_offset():
    store = MemoryStore2()
    descr = {
        "dimensions": {"time": 0, "cell": 12},
        "chunks": [{"time": 11}, {"time": 1, "cell": 12}],
        "encoding": default_encoding,
        "variables": {
            "time": timevar_descr,
            "test": ["time", "cell"],
        }
    }
    init_zarr(descr, store=store)
    extend_time(store, "2023-01-01T00:00:00", "2023-01-01T00:00:10", "PT1S")

    run_output(component=DummyTransmitter(source=DummySource(time_offset=10)),
               gridspec=HealPixSubgridDefinition(1, 1, 0),
               varnames=["test"],
               store=store)

    testvar = zarr.open_group(store)["test"]
    assert np.all(testvar[-1] == np.arange(12, dtype="<f8"))
    assert np.all(np.isnan(testvar[:-1]))


@pytest.mark.parametrize("chunksize", [4,5])
def test_output_with_time_chunks(chunksize):
    store = MemoryStore2()
    descr = {
        "dimensions": {"time": 0, "cell": 12},
        "chunks": [{"time": 8}, {"time": chunksize, "cell": 12}],
        "encoding": default_encoding,
        "variables": {
            "time": timevar_descr,
            "test": ["time", "cell"],
        }
    }
    init_zarr(descr, store=store)
    extend_time(store, "2023-01-01T00:00:00", "2023-01-01T00:00:07", "PT1S")

    run_output(component=DummyTransmitter(source=DummySource(8)),
               gridspec=HealPixSubgridDefinition(1, 1, 0),
               varnames=["test"],
               store=store)

    non_meta_keys = set(k for k in store if not k.split("/")[-1].startswith(".z"))
    assert non_meta_keys == {"time/0", "test/0/0", "test/1/0"}

    testvar = zarr.open_group(store)["test"]
    assert np.all(testvar[:] == np.arange(12, dtype="<f8"))
    assert testvar.shape == (8, 12)


@pytest.mark.parametrize("chunksize", [4,5])
def test_output_with_vertical_chunks(chunksize):
    store = MemoryStore2()
    descr = {
        "dimensions": {"time": 0, "height": 8, "cell": 12},
        "chunks": [{"time": 2}, {"time": 1, "height": chunksize, "cell": 12}],
        "encoding": default_encoding,
        "variables": {
            "time": timevar_descr,
            "test3d": ["time", "height", "cell"],
        }
    }
    init_zarr(descr, store=store)
    extend_time(store, "2023-01-01T00:00:00", "2023-01-01T00:00:01", "PT1S")

    test_array = np.arange(8*12).reshape(8,12)
    run_output(component=DummyTransmitter(source=DummySource(2, lambda *_: np.array(test_array, copy=True))),
               gridspec=HealPixSubgridDefinition(1, 1, 0),
               varnames=["test3d"],
               store=store)


    non_meta_keys = set(k for k in store if not k.split("/")[-1].startswith(".z"))
    assert non_meta_keys == {"time/0", "test3d/0/0/0", "test3d/1/0/0", "test3d/0/1/0", "test3d/1/1/0"}

    testvar = zarr.open_group(store)["test3d"]
    assert np.all(testvar[:] == test_array.astype("<f8"))
    assert testvar.shape == (2, 8, 12)


@pytest.mark.parametrize("chunksize", [4,5])
def test_output_with_vertical_and_time_chunks(chunksize):
    store = MemoryStore2()
    descr = {
        "dimensions": {"time": 0, "height": 8, "cell": 12},
        "chunks": [{"time": 2}, {"time": 2, "height": chunksize, "cell": 12}],
        "encoding": default_encoding,
        "variables": {
            "time": timevar_descr,
            "test3d": ["time", "height", "cell"],
        }
    }
    init_zarr(descr, store=store)
    extend_time(store, "2023-01-01T00:00:00", "2023-01-01T00:00:01", "PT1S")

    test_array = np.arange(8*12).reshape(8,12)
    run_output(component=DummyTransmitter(source=DummySource(2, lambda *_: np.array(test_array, copy=True))),
               gridspec=HealPixSubgridDefinition(1, 1, 0),
               varnames=["test3d"],
               store=store)

    non_meta_keys = set(k for k in store if not k.split("/")[-1].startswith(".z"))
    assert non_meta_keys == {"time/0", "test3d/0/0/0", "test3d/0/1/0"}

    testvar = zarr.open_group(store)["test3d"]
    assert np.all(testvar[:] == test_array.astype("<f8"))
    assert testvar.shape == (2, 8, 12)


def run_aggregation_chain(stores, cellsizes, mk_descr, in_field):
    descrs = [mk_descr(cellsize) for cellsize in cellsizes]

    for descr, store in zip(descrs, stores):
        init_zarr(descr, store=store)
        extend_time(store, "2023-01-01T00:00:00", "2023-01-01T00:00:00", "PT1S")

    pipes = [MessagePipe() for _ in cellsizes[:-1]]
    sources = [DummySource(1, lambda *_: np.array([in_field]))] + pipes
    sinks = pipes + [None]

    transmitters = [DummyTransmitter(source=source, sink=sink)
                    for source, sink in zip(sources, sinks)]

    threads = []

    def worker(transmitter, store, hcoarsen_level):
        run_output(component=transmitter,
                   gridspec=DummyGridspec(),
                   varnames=["test"],
                   store=store,
                   hcoarsen_level=hcoarsen_level,
                   output_hcoarsen_step=1 if transmitter.sink else 0)

        if transmitter.sink:
            transmitter.sink.join()

    for hcoarsen_level, (transmitter, store) in enumerate(zip(transmitters, stores)):
        t = threading.Thread(target=worker,
                             kwargs=dict(
                               transmitter=transmitter,
                               store=store,
                               hcoarsen_level=hcoarsen_level),
                             )
        t.start()
        threads.append(t)

    for t in threads:
        t.join()


@pytest.mark.parametrize("in_field,out_field", [([1., 2., 3., np.nan], [2.]),
                                                ([np.nan] * 4, [np.nan])])
def test_aggregation_with_nan(in_field, out_field):
    cellsizes = [4, 1]
    stores = [MemoryStore2() for _ in cellsizes]
    def mk_descr(cellsize):
        return {
                  "dimensions": {"time": 0, "cell": cellsize},
                  "chunks": [{"time": 1}, {"time": 1, "cell": 4}],
                  "encoding": default_encoding,
                  "variables": {
                      "time": timevar_descr,
                      "test": ["time", "cell"],
                  }
               }

    run_aggregation_chain(stores, cellsizes, mk_descr, in_field)

    testvar = zarr.open_group(stores[-1])["test"]
    npt.assert_array_equal(testvar, np.array([out_field]))

# test follows https://gitlab.gwdg.de/ican/hiopy/-/issues/10
@pytest.mark.parametrize("in_field,out_field", [([1., np.nan, 2., 3.,
                                                  np.nan, np.nan, np.nan, np.nan,
                                                  4., 5., 7., 8.,
                                                  6., np.nan, 9., np.nan], [5.])])
def test_multi_level_aggregation_with_nan(in_field, out_field):
    cellsizes = [16, 4, 1]
    stores = [MemoryStore2() for _ in cellsizes]
    def mk_descr(cellsize):
        return {
                  "dimensions": {"time": 0, "cell": cellsize},
                  "chunks": [{"time": 1}, {"time": 1, "cell": 4}],
                  "encoding": default_encoding,
                  "variables": {
                      "time": timevar_descr,
                      "test": ["time", "cell"],
                  }
               }

    run_aggregation_chain(stores, cellsizes, mk_descr, in_field)

    testvar = zarr.open_group(stores[-1])["test"]
    npt.assert_array_equal(testvar, np.array([out_field]))
