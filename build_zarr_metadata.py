import json
import os
import zarr


def create_variable(varname, shape, chunks):
    return {
        "chunks": list(chunks),
        "compressor": {"id": "blosc", "cname": "zstd", "clevel": 5, "shuffle": 1},
        "dtype": "<f8",
        "fill_value": "NaN",
        "filters": None,
        "order": "C",
        "shape": list(shape),
        "dimension_separator": "/",
        "zarr_format": 2,
    }


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("basedir")
    parser.add_argument("-b", "--bisections", type=int)
    parser.add_argument("-c2", "--chunks_2d", type=int, default=1)
    parser.add_argument("-c3", "--chunks_3d", type=int, default=1)
    parser.add_argument("-t", "--ntime", type=int, default=48)
    args = parser.parse_args()

    # basedir = "/scratch/m/m300827/icon_test/yhp0001/work/run_20200120T000000-20200120T235920/result.zarr/"
    basedir = args.basedir
    vars2d = ["10m_wind_speed", "clivi", "cllvi", "sea_level_pressure", "tas"]
    vars3d = ["cli", "clw"]
    ntime = args.ntime
    b = args.bisections
    shape2d = [ntime, 12 * 4 ** b]
    shape3d = [ntime, 90, 12 * 4 ** b]
    chunks2d = [1, 12 * 4 ** b // args.chunks_2d]
    chunks3d = [1, 10, 12 * 4 ** b // args.chunks_3d]
    attrs2d = {"_ARRAY_DIMENSIONS": ["time", "pix"]}
    attrs3d = {"_ARRAY_DIMENSIONS": ["time", "level", "pix"]}

    for var in vars2d:
        with open(os.path.join(basedir, var, ".zarray"), "w") as fp:
            json.dump(create_variable(var, shape2d, chunks2d), fp)
        with open(os.path.join(basedir, var, ".zattrs"), "w") as fp:
            json.dump(attrs2d, fp)

    for var in vars3d:
        with open(os.path.join(basedir, var, ".zarray"), "w") as fp:
            json.dump(create_variable(var, shape3d, chunks3d), fp)
        with open(os.path.join(basedir, var, ".zattrs"), "w") as fp:
            json.dump(attrs3d, fp)

    with open(os.path.join(basedir, ".zgroup"), "w") as fp:
        json.dump({"zarr_format": 2}, fp)

    zarr.consolidate_metadata(basedir)


if __name__ == "__main__":
    exit(main())
