# hiopy

`hiopy` writes model output in chunks, parallel.

Detailed documentation can be found [online](https://ican.pages.gwdg.de/hiopy/), or can be build locally using sphinx:

```
pip install .[docs]
cd docs
make html
open _build/html/index.html
```

## important notes

Currently, `timelag != 0` can lead to infinite waiting times.
