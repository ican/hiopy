import time


class Timer:
    def __init__(self):
        self.invocations = 0
        self.time_spent = 0
        self.last_start = None

    def start(self):
        self.invocations += 1
        self.last_start = time.perf_counter()

    def stop(self):
        self.time_spent += time.perf_counter() - self.last_start
        self.last_start = None

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()
