import numpy as np
import healpy

from .base import GridDescriptorBase


class HealPixSubgridDefinition(GridDescriptorBase):
    def __init__(self, nside, nchunks, ichunk, nest=True):
        super().__init__(nchunks=nchunks, ichunk=ichunk)

        self.nside = nside
        self.nest = nest
        assert self.total_number_of_cells % self.nchunks == 0

    @property
    def grid_id(self):
        if self.nest:
            kind = "nest"
        else:
            kind = "ring"
        return f"healpix_{self.nside}_{self.nchunks}_{self.ichunk}_{kind}"

    @property
    def total_number_of_cells(self):
        return healpy.pixelfunc.nside2npix(self.nside)

    def get_geometry(self):
        boundaries_xyz = (
            healpy.boundaries(self.nside, self.global_index, nest=self.nest)
            .transpose(0, 2, 1)
            .reshape(-1, 3)
        )
        verts_xyz, quads = np.unique(boundaries_xyz, return_inverse=True, axis=0)
        return verts_xyz, quads.reshape(-1, 4)

    def get_centers_xyz(self):
        return np.stack(
            healpy.pixelfunc.pix2vec(self.nside, self.global_index, nest=self.nest),
            axis=-1,
        )

    def coarsen(self, steps):
        """
        Make a coarser version of this grid, coarsened by `steps` steps.
        """
        return HealPixSubgridDefinition(
            self.nside // 2, self.nchunks, self.ichunk, self.nest
        )
