import numpy as np


# NOTE: this module uses lon/lat coordinates in DEGREES

def lonlat2xyz(lon, lat):
    lon = np.deg2rad(lon)
    lat = np.deg2rad(lat)
    clat = np.cos(lat)
    slat = np.sin(lat)
    return np.stack([clat * np.cos(lon), clat * np.sin(lon), slat], axis=-1)

def xyz2lonlat(xyz):
    xyz = np.array(xyz)
    lat = np.arcsin(xyz[..., 2])
    lon = np.arctan2(xyz[..., 1], xyz[..., 0])
    return np.rad2deg(np.stack([lon, lat], axis=-1))


class GridDescriptorBase:
    def __init__(self, nchunks=1, ichunk=0):
        self.nchunks = nchunks
        self.ichunk = ichunk
        assert self.ichunk < self.nchunks

    @property
    def cells_per_chunk(self):
        return int(np.ceil(self.total_number_of_cells / self.nchunks))

    @property
    def global_index(self):
        cpc = self.cells_per_chunk
        return np.arange(cpc) + self.ichunk * cpc

    @property
    def cell_chunk_slice(self):
        return slice(self.ichunk * self.cells_per_chunk, (self.ichunk + 1) * self.cells_per_chunk)

    def get_cells(self):
        return self.get_geometry()[1]

    def get_vert_xyz(self):
        return self.get_geometry()[0]

    def get_vert_lonlat(self):
        return xyz2lonlat(self.get_vert_xyz())

    def get_centers_lonlat(self):
        return xyz2lonlat(self.get_centers_xyz())
