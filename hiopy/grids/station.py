import numpy as np

from .base import GridDescriptorBase, lonlat2xyz


class StationGridDefinition(GridDescriptorBase):
    def __init__(self, stations, nchunks=1, ichunk=0):
        super().__init__(nchunks=nchunks, ichunk=ichunk)

        self.stations = np.array(stations)
        assert len(self.stations.shape) == 2
        assert self.stations.shape[1] == 2

    @property
    def total_number_of_cells(self):
        return len(self.stations)

    @property
    def _chunk_stations(self):
        cpc = self.cells_per_chunk
        ofs = self.ichunk * cpc 
        return self.stations[ofs: ofs+cpc]

    def get_geometry(self):
        verts_xyz = lonlat2xyz(*self._chunk_stations.T)
        cells = np.arange(self.cells_per_chunk)[:, np.newaxis]
        return verts_xyz, cells
    
    def get_centers_xyz(self):
        return lonlat2xyz(*self._chunk_stations.T)

    def coarsen(self, steps):
        raise RuntimeError("StationGrid can not be coarsened")
