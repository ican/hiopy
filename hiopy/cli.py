import sys
import logging
import yaml

from .utils.filter_unwanted import configure_filter_unwanted_parser
from .utils.fill_placeholder import configure_fill_placeholder_parser
from .utils.simple2complex_coupling import configure_s2c_parser, simple2complex
from .utils.build_coupling import configure_c2x_parser, complex2xml
from .utils.multilevel_coupling import configure_multilevel_coupling_parser
from .utils.merge_coupling import configure_merge_couplings_parser
from .utils.healpix_output import configure_healpix_output_parser
from .utils.meteogram_output import configure_meteogram_output_parser
from .utils.init_zarr import configure_init_zarr_parser
from .utils.extend_time import configure_extend_time_parser
from .utils.configure import configure_configure_parser


def configure_s2x_parser(parser):
    parser.add_argument("-i", "--infile", default="-")
    parser.add_argument("-o", "--outfile", default="-")

    def run(args):
        if args.infile == "-":
            infile = sys.stdin
        else:
            infile = open(args.infile)

        if args.outfile == "-":
            outfile = sys.stdout
        else:
            outfile = open(args.outfile, "w")

        d = yaml.load(infile, Loader=yaml.SafeLoader)
        x = complex2xml(simple2complex(d))
        x.write(outfile, encoding="unicode", xml_declaration=True)

    parser.set_defaults(func=run)

def configure_version_parser(parser):
    def run(args):
        from ._version import __version__
        print(f"hiopy version {__version__}")

    parser.set_defaults(func=run)


def get_parser():
    import argparse

    parser = argparse.ArgumentParser(
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "-v",
        "--verbose",
        metavar="DEBUG",
        help="Set the level of verbosity [DEBUG, INFO, WARNING, ERROR]",
        required=False,
        default="INFO",
    )

    parser.set_defaults(func=None)
    subparsers = parser.add_subparsers()
    configure_version_parser(subparsers.add_parser('version'))
    configure_s2c_parser(subparsers.add_parser('s2c'))
    configure_c2x_parser(subparsers.add_parser('c2x'))
    configure_s2x_parser(subparsers.add_parser('s2x'))
    configure_filter_unwanted_parser(subparsers.add_parser('filter_unwanted_coupling'))
    configure_fill_placeholder_parser(subparsers.add_parser('fill_placeholder'))
    configure_multilevel_coupling_parser(subparsers.add_parser('multilevel_coupling'))
    configure_merge_couplings_parser(subparsers.add_parser('merge_couplings'))
    configure_healpix_output_parser(subparsers.add_parser('healpix_output', aliases=['ho']))
    configure_meteogram_output_parser(subparsers.add_parser('meteogram_output', aliases=['mo']))
    configure_init_zarr_parser(subparsers.add_parser('init_zarr'))
    configure_extend_time_parser(subparsers.add_parser('extend_time'))
    configure_configure_parser(subparsers.add_parser('configure'))

    return parser

def main():
    args = get_parser().parse_args()

    logging.basicConfig(level=logging.getLevelName(args.verbose))

    if not args.func:
        logging.error("please provide a subcommand, run with -h for help")
        return -1

    return(args.func(args))

if __name__ == "__main__":
    exit(main())
