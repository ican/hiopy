#!/usr/bin/env python3
import argparse
import json
import yaml

import fsspec


def init_zarr(desc, store=None):
    if store is None:
        store = fsspec.get_mapper(desc["store"])

    zmetadata = {}
    zmetadata[".zgroup"] = {"zarr_format": 2}
    zmetadata[".zattrs"] = desc.get("attrs", {})

    chunks_by_dims = {tuple([d]): tuple([s]) for d, s in desc["dimensions"].items()}
    chunks_by_dims.update(dict(zip(*sorted(c.items())) for c in desc["chunks"]))
    chunks_by_dims[()] = ()

    def _normalize_vardesc(d):
        if isinstance(d, dict):
            return d
        else:
            return {"dims": d, "attrs": {}}

    for var, vardesc in desc["variables"].items():
        vardesc = _normalize_vardesc(vardesc)
        sdims = tuple(sorted(vardesc["dims"]))
        chunk_sizes = dict(zip(sdims, chunks_by_dims[sdims]))

        zmetadata[f"{var}/.zarray"] = {
            "shape": [desc["dimensions"][d] for d in vardesc["dims"]],
            "chunks": [chunk_sizes[d] for d in vardesc["dims"]],
            "order": "C",
            "dimension_separator": "/",
            "zarr_format": 2,
            **desc["encoding"],
            **vardesc.get("encoding", {}),
        }

        zmetadata[f"{var}/.zattrs"] = {
            "_ARRAY_DIMENSIONS": vardesc["dims"],
            **vardesc["attrs"],
        }

        if "cell" in vardesc["dims"]:
            zmetadata[f"{var}/.zattrs"].update(grid_mapping="crs")

    zmetadata[".zmetadata"] = {"zarr_consolidated_format": 1, "metadata": zmetadata.copy()}
    for path, metadata in zmetadata.items():
        store[path] = json.dumps(metadata, indent=2).encode()


def configure_init_zarr_parser(parser):
    parser.add_argument("filename", help="YAML file with dataset description")
    parser.add_argument("-o", "--output", help="path to store to be initialized (if not given it's read from the dataset description)")
    parser.set_defaults(func=_run)


def _run(args):
    if args.output is None:
        store = None
    else:
        store = fsspec.get_mapper(args.output)
    with open(args.filename, "r") as fp:
        init_zarr(yaml.safe_load(fp), store=store)
