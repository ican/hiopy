import numpy as np

def variable_args(parser):
    parser.add_argument(
        "-v", "--varname", nargs="+", type=str, help="variable name to write"
    )


def tchunk_args(parser):
    parser.add_argument(
        "--index_offset", type=int, default=0, help="offset for output time chunk index"
    )
    parser.add_argument(
        "--time_chunk_size",
        type=int,
        default=1,
        help="time chunk size",
    )


def hchunk_args(parser):
    parser.add_argument(
        "-n", "--nchunks", type=int, default=1, help="number of spatial chunks"
    )
    parser.add_argument(
        "-i", "--ichunk", type=int, default=0, help="chunk to extract data for"
    )

def vchunk_args(parser):
    parser.add_argument(
        "-t",
        "--vertical_chunk_size",
        type=int,
        default=None,
        help="enable vertical dimension, use this chunk size",
    )


def hcoarsen_args(parser):
    parser.add_argument(
        "-l", "--hlevel", type=int, default=0, help="horizontal coarsening level (0 == finest)"
    )
    parser.add_argument(
        "-s", "--hstep", type=int, default=0, help="horizontal coarsening step for republishing  (0 == off)"
    )


def output_args(parser):
    parser.add_argument(
        "-o", "--output", type=str, help="output_path (fsspec compatible)"
    )



def yac_args(parser):
    parser.add_argument(
        "-c",
        "--component_name",
        type=str,
        default="healpix_io",
        help="YAC component name",
    )
    parser.add_argument(
        "-g",
        "--gridname",
        type=str,
        default="healpix",
        help="grid basename",
    )
    parser.add_argument(
        "--xml", type=str, default="coupling.xml", help="coupling XML configuration"
    )
    parser.add_argument(
        "--xsd", type=str, default="coupling.xsd", help="coupling XSD document"
    )
    parser.add_argument(
        "--yac_lag", type=int, default=0, help="lag of this YAC component"
    )
