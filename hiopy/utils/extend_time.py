#!/usr/bin/env python3
import argparse
import json

import fsspec
import isodate
import numpy as np
import zarr


def create_time(start, stop, step, refdate="1970-01-01T00:00:00", lag=0):
    """Create a time array."""
    start = np.datetime64(start, "s")
    stop = np.datetime64(stop, "s")
    step = np.timedelta64(isodate.parse_duration(step), "s")

    time = np.arange(start + lag * step, stop + step, step)
    return (time - np.datetime64(refdate, "s")).astype(np.int64)


def extend_time(store, initial_date, end_date, file_interval, lag=0, force=False):
    time_new = create_time(
        start=initial_date.lstrip("+").rstrip("Z"),
        stop=end_date.lstrip("+").rstrip("Z"),
        step=file_interval,
        refdate="1970-01-01T00:00:00",
        lag=lag,
    )

    zmetadata = json.loads(store[".zmetadata"])["metadata"]

    zroot = zarr.open_consolidated(store)

    time_old = zroot["time"][:]
    if not np.array_equal(time_old, time_new[: time_old.size]) and not force:
        raise ValueError("New time values do not extend the existing ones.")

    for var in zroot:
        zattrs = f"{var}/.zattrs"
        zarray = f"{var}/.zarray"
        if zattrs in zmetadata and zarray in zmetadata:
            if "time" in zmetadata[zattrs]["_ARRAY_DIMENSIONS"]:
                i = zmetadata[zattrs]["_ARRAY_DIMENSIONS"].index("time")
                zmetadata[zarray]["shape"][i] = time_new.size

    zmetadata["time/.zarray"]["chunks"] = [time_new.size]

    zmetadata[".zmetadata"] = {
        "zarr_consolidated_format": 1,
        "metadata": zmetadata.copy(),
    }
    store.setitems({p: json.dumps(c, indent=2).encode() for p, c in zmetadata.items()})

    zroot = zarr.open_consolidated(store)
    zroot["time"][:] = time_new


def configure_extend_time_parser(parser):
    parser.add_argument("store", help="Zarr store")
    parser.add_argument("start", help="Initial date")
    parser.add_argument("stop", help="Final date")
    parser.add_argument("step", help="Time increment")
    parser.add_argument(
        "-f", "--force",
        help="Overwrite an existing time axis",
        action="store_true",
    )
    parser.add_argument(
        "-l", "--lag",
        help="timelag: skip that many timesteps at the beginning",
        type=int,
        default=0,
    )
    parser.set_defaults(func=_run)


def _run(args):
    extend_time(
        store=fsspec.get_mapper(args.store),
        initial_date=args.start,
        end_date=args.stop,
        file_interval=args.step,
        lag=args.lag,
        force=args.force,
    )
