import yaml
import sys
from functools import reduce
from .simple2complex_coupling import normalize_varlist

def listify_varlist(vardict):
    return [
        k if v == {"size": 1} or v == {} else {k: v}
        for k, v in vardict.items()
    ]


def merge_two_components(a, b, name):
    if a is None:
        a = {}
    if b is None:
        b = {}

    simple_fields = ["model", "simulated", "grid", "time"]
    for f in simple_fields:
        if f in a and f in b:
            assert a[f] == b[f], f"entry {f} has conflicting definitions for component {name}"
    component = {
        **{f: a[f] for f in simple_fields if f in a},
        **{f: b[f] for f in simple_fields if f in b},
    }

    a_have = normalize_varlist(a.get("have", []))
    b_have = normalize_varlist(b.get("have", []))
    a_want = normalize_varlist(a.get("want", []))
    b_want = normalize_varlist(b.get("want", []))

    for k in set(a_have) & set(b_have):
        assert a_have[k] == b_have[k], f"conflicting havelist entry {k} for component {name}"

    for k in set(a_want) & set(b_want):
        assert a_want[k] == b_want[k], f"conflicting wantlist entry {k} for component {name}"

    havelist = listify_varlist({**a_have, **b_have})
    wantlist = listify_varlist({**a_want, **b_want})

    if havelist:
        component["have"] = havelist

    if wantlist:
        component["want"] = wantlist

    return component or None


def merge_two_couplings(a, b):
    a_inter = a.get("interpolations", {})
    b_inter = b.get("interpolations", {})

    for interpolation in set(a_inter) | set(b_inter):
        if interpolation in a_inter and interpolation in b_inter:
            assert a_inter[interpolation] == b_inter[interpolation], f"interpolation {interpolation} has conflicting definitions!"

    interpolations = {**a_inter, **b_inter}

    a_comp = a.get("components", {})
    b_comp = b.get("components", {})

    components = {
        name: merge_two_components(a_comp.get(name, {}), b_comp.get(name, {}), name)
        for name in sorted(set(a_comp) | set(b_comp))
    }

    other_fields = sorted((set(a) | set(b)) - {"interpolations", "components"})

    for f in other_fields:
        if f in a and f in b:
            assert a[f] == b[f], f"entry {f} has conflicting definitions"

    return {
        **{f: a[f] for f in other_fields if f in a},
        **{f: b[f] for f in other_fields if f in b},
        "components": components,
        "interpolations": interpolations,
    }



def merge_couplings(configs):
    return reduce(merge_two_couplings, configs)


def get_infile(name):
    if name == "-":
        return sys.stdin
    else:
        return open(name)


def run(args):
    if args.outfile == "-":
        outfile = sys.stdout
    else:
        outfile = open(args.outfile, "w")

    configs = [yaml.load(get_infile(name), Loader=yaml.SafeLoader) for name in args.infiles]
    config = merge_couplings(configs)
    yaml.dump(config, outfile, sort_keys=False)


def configure_merge_couplings_parser(parser):
    parser.add_argument("-i", "--infiles", nargs="+")
    parser.add_argument("-o", "--outfile", default="-")
    parser.set_defaults(func=run)

if __name__ == "__main__":
    main()
