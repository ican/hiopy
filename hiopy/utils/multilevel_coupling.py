import yaml
import sys
from .simple2complex_coupling import normalize_varlist
from .healpix_output import format_levelname
from ..aggregation import get_aggregation


MULTILEVEL_INTERPOLATION = {
    "space": [{
        "method": "n-nearest_neighbor",
        "n": 1,
        "weighted": "ARITHMETIC_AVERAGE"
    }],
    "time": "none"
}


def build_multilevel_coupling(config, target_level, aggregation=None):
    aggregation = aggregation or get_aggregation("nanmean")
    var_haves = {k: v
        for c in config["components"].values()
        if c
        for k, v in normalize_varlist(c.get("have", [])).items()
    }

    def build_components():
        def optional_size(size):
            if size != 1:
                return {"size": size}
            else:
                return {}

        for name, component in config["components"].items():
            if not component or component.get("model", "") != "output":
                yield name, component
            else:
                stream_id = name
                for t in range(target_level + 1):
                    if t < target_level:
                        have = {
                            format_levelname(w, t+1, stream_id): {
                                **var_haves[w],
                                "grid": format_levelname(component["grid"] + "_c", t+1),
                                **optional_size(var_haves[w].get("size", 1) * aggregation.buffer_stretch_factor),
                            } for w in normalize_varlist(component["want"])
                        }
                    else:
                        have = {}

                    if t == 0:
                        interp_opts = {}
                    else:
                        interp_opts = {"interpolation": "__hiopy_multilevel"}

                    yield format_levelname(name, t), {
                            **component,
                            "grid": format_levelname(component["grid"], t),
                            "want": {format_levelname(w, t, stream_id): {**opts, **interp_opts}
                                for w, opts in normalize_varlist(component["want"]).items()},
                            "have": have,
                            }

    return {
            **config,
            "components": dict(build_components()),
            "interpolations": {**config.get("interpolations", {}), "__hiopy_multilevel": MULTILEVEL_INTERPOLATION},
    }


def run(args):
    config = yaml.load(sys.stdin, Loader=yaml.SafeLoader)
    config = build_multilevel_coupling(config, args.target_level, args.aggregation)
    yaml.dump(config, sys.stdout, sort_keys=False)


def configure_multilevel_coupling_parser(parser):
    parser.add_argument("-t", "--target_level", type=int, default=0, help="maximum coarsening level")
    parser.add_argument("--aggregation", type=get_aggregation, default=get_aggregation("nanmean"), help="aggregation function to use")
    parser.set_defaults(func=run)
