import sys
import xml.etree.ElementTree as ET
import yaml


def dict2xml(d):
    grid_refs = {name: str(i) for i, name in enumerate(d["grids"], 1)}
    transient_refs = {name: str(i) for i, name in enumerate(d["transients"], 1)}
    component_of_transient_grid = {}

    xroot = ET.Element(
        "coupling",
        attrib={
            "xmlns": "http://www.w3schools.com",
            "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
            "xsi:schemaLocation": "http://www.w3schools.com coupling.xsd",
        },
    )
    r = d.get("redirect", {})
    ET.SubElement(
        xroot,
        "redirect",
        attrib={
            key: "true" if r.get(key, False) else "false"
            for key in ["redirect_of_root", "redirect_stdout"]
        },
    )
    xcomponents = ET.SubElement(xroot, "components")
    component_ids = {}
    for i, (cname, component) in enumerate(d.get("components", {}).items(), 1):
        component_ids[cname] = str(i)
        xcomponent = ET.SubElement(xcomponents, "component", attrib={"id": str(i)})
        xname = ET.SubElement(xcomponent, "name")
        xname.text = cname
        xmodel = ET.SubElement(xcomponent, "model")
        xmodel.text = component.get("model", "")
        xsimulated = ET.SubElement(xcomponent, "simulated")
        xsimulated.text = component.get("simulated", "")

        xtrefs = ET.SubElement(xcomponent, "transient_grid_refs")
        for j, (tname, tref) in enumerate(
            component.get("transient_grid_refs", {}).items(), 1
        ):
            ET.SubElement(
                xtrefs,
                "transient_grid_ref",
                attrib={
                    "collection_size": str(tref["size"]),
                    "grid_ref": grid_refs[tref["grid"]],
                    "id": str(j),
                    "transient_ref": transient_refs[tref["transient"]],
                },
            )
            component_of_transient_grid[tname] = cname, str(j), tref["transient"]

    xtransients = ET.SubElement(xroot, "transients")
    for i, tname in enumerate(d["transients"], 1):
        ET.SubElement(
            xtransients,
            "transient",
            attrib={"id": str(i), "transient_standard_name": tname},
        )

    xgrids = ET.SubElement(xroot, "grids")
    for i, gname in enumerate(d["grids"], 1):
        ET.SubElement(xgrids, "grid", attrib={"id": str(i), "alias_name": gname})

    xdate = ET.SubElement(xroot, "dates")
    xstart_date = ET.SubElement(xdate, "start_date")
    xstart_date.text = d["dates"]["start"]
    xend_date = ET.SubElement(xdate, "end_date")
    xend_date.text = d["dates"]["end"]
    xcalendar = ET.SubElement(xdate, "calendar")
    xcalendar.text = d["dates"]["calendar"]

    xtimestep_unit = ET.SubElement(xroot, "timestep_unit")
    xtimestep_unit.text = d["timestep_unit"]

    xcouples = ET.SubElement(xroot, "couples")
    for couple in d.get("couples", []):
        xcouple = ET.SubElement(xcouples, "couple")
        component_refs = {}
        for i, component in enumerate(couple["components"], 1):
            component_refs[component] = str(i)
            ET.SubElement(
                xcouple, f"component{i}", component_id=component_ids[component]
            )
        for i, tcouple in enumerate(couple["transient_couples"], 1):
            scname, sctgref, stransient = component_of_transient_grid[
                tcouple["source"]["ref"]
            ]
            tcname, tctgref, ttransient = component_of_transient_grid[
                tcouple["target"]["ref"]
            ]
            assert stransient == ttransient

            xtc = ET.SubElement(
                xcouple, "transient_couple", transient_id=transient_refs[stransient]
            )

            ET.SubElement(
                xtc,
                "source",
                component_ref=component_refs[scname],
                transient_grid_ref=sctgref,
            )
            ET.SubElement(xtc, "target", transient_grid_ref=tctgref)

            xtimestep = ET.SubElement(xtc, "timestep")
            xtss = ET.SubElement(xtimestep, "source")
            xtss.text = tcouple["source"]["timestep"]
            xtst = ET.SubElement(xtimestep, "target")
            xtst.text = tcouple["target"]["timestep"]
            xcoupling_period = ET.SubElement(
                xtimestep,
                "coupling_period",
                operation=tcouple["coupling_period"]["operation"],
            )
            xcoupling_period.text = tcouple["coupling_period"]["interval"]
            for side in ["source", "target"]:
                xtl = ET.SubElement(xtimestep, f"{side}_timelag")
                xtl.text = str(tcouple[side]["timelag"])

            xmos = ET.SubElement(xtc, "mapping_on_source")
            xmos.text = "true" if tcouple["mapping_on_source"] else "false"

            xinterp_req = ET.SubElement(xtc, "interpolation_requirements")
            for interpolation in tcouple.get("interpolation", []):
                ET.SubElement(
                    xinterp_req,
                    "interpolation",
                    attrib={k: str(v) for k, v in interpolation.items()},
                )

            xenf_weight = ET.SubElement(xtc, "enforce_write_weight_file")
            xenf_weight.text = (
                "true" if tcouple.get("write_weight_file", False) else "false"
            )

    ET.SubElement(xroot, "created", tool="build_coupling.py")
    return ET.ElementTree(xroot)


def complex2xml(config):
    x = dict2xml(config)
    try:
        ET.indent(x)
    except AttributeError:
        # indentation is only supported with Python >= 3.9
        # but we don't need it for functionality.
        pass
    return x

def run(args):
    if args.infile == "-":
        infile = sys.stdin
    else:
        infile = open(args.infile)

    if args.outfile == "-":
        outfile = sys.stdout
    else:
        outfile = open(args.outfile, "w")

    d = yaml.load(infile, Loader=yaml.SafeLoader)
    x = complex2xml(d)
    x.write(outfile, encoding="unicode", xml_declaration=True)


def configure_c2x_parser(parser):
    parser.add_argument("-i", "--infile", default="-")
    parser.add_argument("-o", "--outfile", default="-")

    parser.set_defaults(func=run)



def main():
    import argparse

    parser = argparse.ArgumentParser()
    configure_s2c_parser(parser)
    run(parser.parse_args())


if __name__ == "__main__":
    exit(main())
