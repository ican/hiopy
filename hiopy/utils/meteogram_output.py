from pathlib import Path
import yaml
import fsspec
import numcodecs

from . import common_args
from ..pipelines import TimeAlignedZarrOutputPipeline
from ..runner import run_pipelines

def run(args):
    from ..yac_interface import YACComponent
    from ..grids.station import StationGridDefinition
    component = YACComponent(args.component_name, args.xml, args.xsd, lag=args.yac_lag)

    with open(args.station_file) as station_file:
        stations = yaml.load(station_file, Loader=yaml.SafeLoader)

    gridspec = StationGridDefinition([[float(s['lon']), float(s['lat'])] for s in stations],
                                     args.nchunks, args.ichunk)
    store = args.output

    pipelines = [
        TimeAlignedZarrOutputPipeline(
            args.varname,
            store,
            region=(..., gridspec.cell_chunk_slice))]

    listener = component.get_listener(args.gridname, gridspec, args.varname)
    run_pipelines(pipelines, component, listener)


def configure_meteogram_output_parser(parser):
    parser.add_argument("--station_file", type=Path, help="meteogram station description file")

    common_args.variable_args(parser)
    common_args.hchunk_args(parser)
    common_args.output_args(parser)
    common_args.yac_args(parser)

    parser.set_defaults(func=run)
