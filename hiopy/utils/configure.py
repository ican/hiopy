import os
import stat
import sys
import itertools
from collections import defaultdict
import logging
import yaml
import fsspec

from .init_zarr import init_zarr
from .extend_time import extend_time

def grouper(n, iterable):
    iterable = iter(iterable)
    return iter(lambda: list(itertools.islice(iterable, n)), [])


def get_type(o):
    if isinstance(o, str):
        return "str"
    elif isinstance(o, int):
        return "int"
    elif isinstance(o, float):
        return "float"
    else:
        raise ValueError("can't get a string for that type")


def run_coupling(plan, args):
    if args.outfile == "-":
        outfile = sys.stdout
    else:
        outfile = open(args.outfile, "w")

    yaml.dump(plan["couplings"], outfile)


def run_processes(plan, args):
    if args.outfile == "-":
        outfile = sys.stdout
    else:
        outfile = open(args.outfile, "w")

    def gen_processes():
        if args.command_prefix:
            command_prefix = args.command_prefix + " "
        else:
            command_prefix = ""

        pg = plan["process_groups"]
        bisections_by_component = defaultdict(list)
        for g in pg:
            bisections_by_component[g["component"]].append(g["bisections"])

        for g in sorted(pg, key=lambda g: g["group"]):
            for i in range(g["nchunks"]):
                if args.worker_loglevel is None:
                    if i == 0 and (
                            g["bisections"] == max(bisections_by_component[g["component"]]) or
                            g["bisections"] == min(bisections_by_component[g["component"]])
                        ):
                        # first or last process in component
                        loglevel = "INFO"
                    else:
                        loglevel = "WARNING"
                else:
                    loglevel = args.worker_loglevel

                command = ("hiopy -v {loglevel} healpix_output "
                           "-b {bisections} -l {hlevel} -s {hstep} "
                           "-n {nchunks} -i {i} "
                           "-c {component} --yac_lag {lag} -g {grid} "
                           "-v {variables} "
                           "--fpc {fpc} "
                           "-o '{outpath}{group}.zarr'"
                           ).format(**{**g, "i": i, "variables": " ".join(g['variables']), "outpath": args.data_prefix, "fpc": args.fpc, "loglevel": loglevel})
                if args.buffer_entire_timestep:
                    command += " --buffer_entire_timestep"
                if args.output_in_thread:
                    command += " --output_in_thread"
                yield command_prefix + command

    process_groups = list(grouper(args.parallel, gen_processes()))

    if args.scripts is not None:
        for i, ps in enumerate(process_groups):
            filename = os.path.abspath(f"{args.scripts}{i}")
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            with open(filename, "w") as scriptfile:
                print("#!/bin/bash", file=scriptfile)
                if len(ps) == 1:
                    print(ps[0], file=scriptfile)
                else:
                    for p in ps:
                        print(p, "&", file=scriptfile)
                    print("wait", file=scriptfile)

            st = os.stat(filename)
            os.chmod(filename, st.st_mode | stat.S_IEXEC)

            if args.line_numbers is not None:
                line = f"{args.line_numbers + i * args.number_step} {filename}"
            else:
                line = filename

            print(line, file=outfile)
    else:
        for i, ps in enumerate(process_groups):
            if len(ps) == 1:
                line = ps[0]
            else:
                line = "bash -c '" + " & ".join(ps) + "'"

            if args.line_numbers is not None:
                line = f"{args.line_numbers + i * args.number_step} {line}"

            print(line, file=outfile)


def run_datasets(plan, args):
    outpath = args.data_prefix
    for group, dataset in plan["datasets"].items():
        dataset = {**dataset, "chunks": [{"time": 1}] + dataset["chunks"]}
        dataset_path = f"{outpath}{group}.zarr"
        store = fsspec.get_mapper(dataset_path)
        logging.info(f"initializing dataset '{dataset_path}'")
        init_zarr(dataset, store)


def run_extend_time(plan, args):
    outpath = args.data_prefix
    for group, dataset in plan["datasets"].items():
        dataset_path = f"{outpath}{group}.zarr"
        logging.info(f"extending time of dataset '{dataset_path}'")
        extend_time(
            store=fsspec.get_mapper(dataset_path),
            initial_date=args.start,
            end_date=args.stop,
            file_interval=group.split("_")[0],
            lag=1,  # TODO: get from plan
            force=args.force,
        )


def run_dataset_list(plan, args):
    if args.outfile == "-":
        outfile = sys.stdout
    else:
        outfile = open(args.outfile, "w")

    outpath = args.data_prefix
    for group, dataset in plan["datasets"].items():
        dataset_path = f"{outpath}{group}.zarr"
        print(dataset_path, file=outfile)


def run_multilevel_steps(plan, args):
    print(plan["multilevel_steps"])


def run_build_catalog(plan, args):
    if args.outfile == "-":
        outfile = sys.stdout
    else:
        outfile = open(args.outfile, "w")

    outpath = args.data_prefix

    catalog = {
        "sources": {
            args.name: {
                "driver": "zarr",
                "args": {
                    "urlpath": outpath + "_".join("{{ " + r["dim"] + " }}" for r in plan["config"]["resolutions"]) + ".zarr",
                    "chunks": None,
                    "consolidated": True,
                },
                "parameters": {
                    r["dim"]: {
                        "description": f"{r['dim']} resolution of the dataset",
                        "type": get_type(r["values"][0]),
                        "default": r["values"][-1],
                        "allowed": r["values"],
                    }
                    for r in plan["config"]["resolutions"]
                }
            }
        }
    }

    yaml.dump(catalog, outfile)


def run(args):
    if args.plan == "-":
        plan = sys.stdin.read()
    else:
        with fsspec.open(args.plan) as infile:
            plan = infile.read()
    plan = yaml.safe_load(plan)

    return args.kind(plan, args)

def configure_configure_parser(parser):
    parser.add_argument("-p", "--plan", metavar="plan.yaml", default="-", help="expanded dataset plan")
    parser.set_defaults(func=run)

    subparsers = parser.add_subparsers()

    coupling_parser = subparsers.add_parser("coupling")
    coupling_parser.add_argument("-o", "--outfile", default="-")
    coupling_parser.set_defaults(kind=run_coupling)

    processes_parser = subparsers.add_parser("processes")
    processes_parser.add_argument("-o", "--outfile", default="-")
    processes_parser.add_argument("-d", "--data_prefix", default="-", help="prefix for output dataset")
    processes_parser.add_argument("-n", "--line_numbers", default=None, type=int, help="add line numbers and start at this value")
    processes_parser.add_argument("--number_step", default=1, type=int, help="stepsize for counting line numbers")
    processes_parser.add_argument("-p", "--parallel", default=1, type=int, help="run that many processes in parallel")
    processes_parser.add_argument("-s", "--scripts", default=None, type=str, help="create scripts with this file name prefix and call them indirectly")
    processes_parser.add_argument("--command_prefix", default=None, type=str, help="prepend this prefix before each worker command")
    processes_parser.add_argument(
        "--fpc",
        type=int,
        default=1,
        help="flush this many times per chunk, may reduce memory footprint at the expense of re-reading data",
    )
    processes_parser.add_argument(
        "--buffer_entire_timestep",
        default=False,
        action="store_true",
        help="Pull entire timestep as fast as possible from YAC and buffer it before processing.",
    )
    processes_parser.add_argument(
        "--worker_loglevel",
        metavar="DEBUG",
        help="Set the level of verbosity [DEBUG, INFO, WARNING, ERROR] for output workers",
        required=False,
        default=None,
    )
    processes_parser.add_argument(
        "--output_in_thread",
        default=False,
        action="store_true",
        help="Write zarr output in separate thread (reads faster from YAC, but requires more memory)",
    )
    processes_parser.set_defaults(kind=run_processes)

    datasets_parser = subparsers.add_parser("datasets", aliases=["init_zarr"])
    datasets_parser.add_argument("-d", "--data_prefix", default="-", help="prefix for output dataset")
    datasets_parser.set_defaults(kind=run_datasets)

    extend_time_parser = subparsers.add_parser("extend_time")
    extend_time_parser.add_argument("start", help="Initial date")
    extend_time_parser.add_argument("stop", help="Final date")
    extend_time_parser.add_argument("-d", "--data_prefix", default="-", help="prefix for output dataset")
    extend_time_parser.add_argument(
        "-f", "--force",
        help="Overwrite an existing time axis",
        action="store_true",
    )
    extend_time_parser.set_defaults(kind=run_extend_time)

    dataset_list_parser = subparsers.add_parser("dataset_list")
    dataset_list_parser.add_argument("-o", "--outfile", default="-")
    dataset_list_parser.add_argument("-d", "--data_prefix", default="-", help="prefix for output dataset")
    dataset_list_parser.set_defaults(kind=run_dataset_list)

    multilevel_steps_parser = subparsers.add_parser("multilevel_steps")
    multilevel_steps_parser.set_defaults(kind=run_multilevel_steps)

    build_catalog_parser = subparsers.add_parser("build_catalog")
    build_catalog_parser.add_argument("-o", "--outfile", default="-")
    build_catalog_parser.add_argument("-d", "--data_prefix", default="-", help="prefix for output dataset")
    build_catalog_parser.add_argument("-n", "--name", default="result", help="name for the catalog entry")
    build_catalog_parser.set_defaults(kind=run_build_catalog)
