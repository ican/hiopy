import sys
import itertools
import yaml


def str_or_dict(k):
    if isinstance(k, str):
        return k, {}
    else:
        return next(iter(k.items()))


def normalize_varlist(varlist):
    if isinstance(varlist, list):
        return dict(map(str_or_dict, varlist))
    else:
        return varlist


def get_nested_from_first(objects, key):
    for o in objects:
        try:
            for k in key:
                o = o[k]
            return o
        except KeyError:
            continue
    else:
        raise KeyError(f"can't find {key} in any object")


def gen_transientcouples(c1name, c1, c2name, c2, interpolations):
    def gen(k, sname, s, tname, t):
        interpolation = interpolations.get(
            t["want"][k].get("interpolation", t.get("interpolation", "default"))
        )
        yield {
            "source": {
                "ref": f"{k}_{sname}",
                "timestep": get_nested_from_first([s["have"][k], s], ["time", "step"]),
                "timelag": get_nested_from_first([s["have"][k], s], ["time", "lag"]),
            },
            "target": {
                "ref": f"{k}_{tname}",
                "timestep": get_nested_from_first([t["want"][k], t], ["time", "step"]),
                "timelag": get_nested_from_first([t["want"][k], t], ["time", "lag"]),
            },
            "coupling_period": {
                "interval": t["want"][k].get("interval", t.get("time").get("step")),
                "operation": interpolation.get("time"),
            },
            "mapping_on_source": interpolation.get("mapping_on_source", True),
            "interpolation": interpolation.get("space"),
            "write_weight_file": interpolation.get("write_weight_file", False),
        }

    for k in sorted(set(c1["have"]) & set(c2["want"])):
        yield from gen(k, c1name, c1, c2name, c2)

    for k in sorted(set(c2["have"]) & set(c1["want"])):
        yield from gen(k, c2name, c2, c1name, c1)


def gen_couples(simple):
    for (c1name, c1), (c2name, c2) in itertools.combinations(
        simple.get("components", {}).items(), 2
    ):
        tcs = list(
            gen_transientcouples(c1name, c1, c2name, c2, simple["interpolations"])
        )
        if tcs:
            yield {
                "components": [c1name, c2name],
                "transient_couples": tcs,
            }


def simple2complex(simple):
    all_haves = set()
    all_wants = set()
    for c in list(simple["components"]):
        if simple["components"][c] is None:
            simple["components"][c] = {}
    for c in simple.get("components").values():
        c["have"] = normalize_varlist(c.get("have", []))
        c["want"] = normalize_varlist(c.get("want", []))
        all_haves |= set(c["have"])
        all_wants |= set(c["want"])

    missing_variables = all_wants - all_haves
    if missing_variables:
        raise ValueError(f"variables {missing_variables} are wanted but no component has them")

    grids = set()
    transients = {}
    for v in simple.get("components", {}).values():
        if "grid" in v:
            grids.add(v.get("grid"))
        for varname, varconfig in v["have"].items():
            if "grid" in varconfig:
                grids.add(varconfig.get("grid"))
            # assert (
            #     varname not in transients
            # ), f"{varname} is part of multiple have-lists"
            transients[varname] = {"size": varconfig.get("size", 1)}
        for varname, varconfig in v["want"].items():
            if "grid" in varconfig:
                grids.add(v.get("grid"))

    return {
        "redirect": {
            "redirect_of_root": False,
            "redirect_stdout": False,
        },
        "components": {
            k: {
                "model": v.get("model", "none"),
                "simulated": v.get("simulated", "none"),
                "transient_grid_refs": {
                    f"{varname}_{k}": {
                        "size": transients[varname]["size"],
                        "grid": varconfig.get("grid", v.get("grid")),
                        "transient": varname,
                    }
                    for varname, varconfig in {**v["have"], **v["want"]}.items()
                },
            }
            for k, v in simple.get("components", {}).items()
        },
        "transients": list(transients),
        "grids": list(sorted(grids)),
        "dates": simple.get(
            "dates",
            {
                "start": "1800-01-01T00:00:00.000",
                "end": "2100-01-01T00:00:00.000",
                "calendar": "proleptic-gregorian",
            },
        ),
        "timestep_unit": simple.get("timestep_unit", "ISO_format"),
        "couples": list(gen_couples(simple)),
    }


def run(args):
    if args.infile == "-":
        infile = sys.stdin
    else:
        infile = open(args.infile)

    if args.outfile == "-":
        outfile = sys.stdout
    else:
        outfile = open(args.outfile, "w")

    simple = yaml.load(infile, Loader=yaml.SafeLoader)
    config = simple2complex(simple)
    yaml.dump(config, outfile, sort_keys=False)


def configure_s2c_parser(parser):
    parser.add_argument("-i", "--infile", default="-")
    parser.add_argument("-o", "--outfile", default="-")
    parser.set_defaults(func=run)


def main():
    import argparse

    parser = argparse.ArgumentParser()
    configure_s2c_parser(parser)
    run(parser.parse_args())


if __name__ == "__main__":
    exit(main())
