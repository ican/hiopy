import yaml
import sys
import re


placeholder_re = re.compile(r"^%\{([^}]+)\}$")


def fill_placeholder(o, replacements):
    if isinstance(o, dict):
        return {k: fill_placeholder(v, replacements) for k, v in o.items()}
    elif isinstance(o, list):
        return [fill_placeholder(v, replacements) for v in o]
    elif isinstance(o, str):
        if m := placeholder_re.match(o):
            return replacements[m.group(1)]
        else:
            return o
    else:
        return 0


def run(args):
    if args.infile == "-":
        infile = sys.stdin
    else:
        infile = open(args.infile)

    if args.outfile == "-":
        outfile = sys.stdout
    else:
        outfile = open(args.outfile, "w")

    replacements = {k: int(v) for k, v in (r.split("=", 1) for r in args.replace)}

    config = yaml.load(infile, Loader=yaml.SafeLoader)
    config = fill_placeholder(config, replacements)
    yaml.dump(config, outfile, sort_keys=False)


def configure_fill_placeholder_parser(parser):
    parser.add_argument("-i", "--infile", default="-")
    parser.add_argument("-o", "--outfile", default="-")
    parser.add_argument("-r", "--replace", nargs="+", help="key value pairs to replace (e.g. atmo.levels=7)")
    parser.set_defaults(func=run)

if __name__ == "__main__":
    main()
