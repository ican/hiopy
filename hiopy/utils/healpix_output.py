import logging
import numcodecs

from ..timer import Timer
from ..pipelines import HCoarsenPipeline, TimeAlignedZarrOutputPipeline
from ..runner import run_pipelines
from ..aggregation import NanMeanAggregation

from . import common_args


def format_levelname(varname, level=0, stream_id=""):
    if level == 0:
        return varname
    else:
        return f"{varname}_{stream_id}_l{level}"


def run_output(
    component,  # e.g. YACComponent
    gridspec,
    varnames,
    store,
    grid_basename="healpix",
    hcoarsen_level=0,
    output_hcoarsen_step=0,
    stream_id="",
    flushes_per_chunk=1,
    output_in_thread=False,
):
    gridname = format_levelname(grid_basename, hcoarsen_level)

    listen_varnames = [format_levelname(name, hcoarsen_level, stream_id) for name in varnames]
    listen_to_real_varnames = dict(zip(listen_varnames, varnames))

    pipelines = []

    aggregation = NanMeanAggregation(4)

    setup_timer = Timer()
    with setup_timer:
        listener = component.get_listener(gridname, gridspec, listen_varnames)

        if output_hcoarsen_step > 0:
            pipelines.append(HCoarsenPipeline(
                grid_basename,
                gridspec,
                varnames,
                output_hcoarsen_step,
                hcoarsen_level,
                component,
                format_levelname,
                aggregation,
                stream_id))

        pipelines.append(TimeAlignedZarrOutputPipeline(
                varnames,
                store,
                region=(..., gridspec.cell_chunk_slice),
                flushes_per_chunk=flushes_per_chunk,
                output_in_thread=output_in_thread,
                ))

    logging.debug(
        "grid setup of component %s took %f s",
        component.name,
        setup_timer.time_spent,
    )

    run_pipelines(pipelines,
                  component,
                  listener,
                  listen_to_real_varnames,
                  aggregation=aggregation,
                  is_initial_aggregation_step=hcoarsen_level == 0)

    logging.debug("done")


def run(args):
    from ..yac_interface import YACComponent
    from ..grids.healpix import HealPixSubgridDefinition

    import sys
    import shlex
    logging.debug("I am %s", " ".join(map(shlex.quote, sys.argv[1:])))

    component_name = format_levelname(args.component_name, args.hlevel)
    component = YACComponent(component_name, args.xml, args.xsd, lag=args.yac_lag)
    gridspec = HealPixSubgridDefinition(2 ** args.side_bisections, args.nchunks, args.ichunk)

    run_output(
        component,
        gridspec,
        args.varname,
        args.output,
        grid_basename=args.gridname,
        hcoarsen_level=args.hlevel,
        output_hcoarsen_step=args.hstep,
        stream_id=args.component_name,
        flushes_per_chunk=args.fpc,
        output_in_thread=args.output_in_thread,
    )


def configure_healpix_output_parser(parser):
    parser.add_argument(
        "-b",
        "--side_bisections",
        type=int,
        default=0,
        help="number of HEALpix side bisections",
    )
    common_args.variable_args(parser)
    common_args.hchunk_args(parser)
    common_args.hcoarsen_args(parser)
    common_args.output_args(parser)
    common_args.yac_args(parser)
    parser.add_argument(
        "--fpc",
        type=int,
        default=1,
        help="flush this many times per chunk, may reduce memory footprint at the expense of re-reading data",
    )
    parser.add_argument(
        "--buffer_entire_timestep",
        default=False,
        action="store_true",
        help="Pull entire timestep as fast as possible from YAC and buffer it before processing.",
    )
    parser.add_argument(
        "--output_in_thread",
        default=False,
        action="store_true",
        help="Write zarr output in separate thread (reads faster from YAC, but requires more memory)",
    )

    parser.set_defaults(func=run)


def main():
    import argparse

    parser = argparse.ArgumentParser(description="healpix output for ICON")
    configure_healpix_output_parser(parser)
    args = parser.parse_args()
    run(args)

    return 0


if __name__ == "__main__":
    exit(main())
