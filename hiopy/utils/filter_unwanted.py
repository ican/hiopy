import yaml
import sys
from copy import deepcopy
from .simple2complex_coupling import normalize_varlist

def filter_unwanted(config, keep=None):
    config = deepcopy(config)
    keep = keep or set()
    var_want = {k for c in config.get("components", {}).values() if c for k in normalize_varlist(c.get("want", []))} | set(keep)

    def filter_havelist(havelist):
        for entry in havelist:
            if isinstance(entry, str):
                if entry in var_want:
                    yield entry
            else:
                d = {k: v for k, v in entry.items() if k in var_want}
                if d:
                    yield d

    for name, component in config.get("components", {}).items():
        if not component:
            continue
        if "have" in component:
            component["have"] = list(filter_havelist(component["have"]))

    return config
                    

def run(args):
    if args.infile == "-":
        infile = sys.stdin
    else:
        infile = open(args.infile)

    if args.outfile == "-":
        outfile = sys.stdout
    else:
        outfile = open(args.outfile, "w")

    config = yaml.load(infile, Loader=yaml.SafeLoader)
    config = filter_unwanted(config, keep=args.keep)
    yaml.dump(config, outfile, sort_keys=False)


def configure_filter_unwanted_parser(parser):
    parser.add_argument("-i", "--infile", default="-")
    parser.add_argument("-o", "--outfile", default="-")
    parser.add_argument("-k", "--keep", nargs="*")
    parser.set_defaults(func=run)

if __name__ == "__main__":
    main()
