import warnings
import logging
import threading
import queue
import numpy as np
import zarr
import xarray as xr

from .timer import Timer
from .stacker import Stacker


class HCoarsenPipeline:
    requires_raw_agg = True

    def __init__(self, grid_basename, gridspec, varnames, step, input_level, component, format_levelname, aggregation, stream_id=""):
        assert step > 0

        emit_level = input_level + step
        emit_gridname = format_levelname(grid_basename + "_c", emit_level)
        emit_varnames = [format_levelname(name, emit_level, stream_id) for name in varnames]
        emit_gridspec = gridspec.coarsen(step)

        self.vartable = dict(zip(varnames, emit_varnames))
        self.aggregation = aggregation
        self.emitter = component.get_emitter(emit_gridname, emit_gridspec, emit_varnames)

        self.emit_timer = Timer()

    def handle_field(self, varname, i, timestamp, buffer):
        logging.debug("handle coarsen %s", varname)
        emit_varname = self.vartable[varname]
        with self.emit_timer:
            self.emitter.emit(emit_varname, i, timestamp, self.aggregation.aggregate(buffer))

    def finish(self):
        pass

    def get_timers(self):
        return {"emit": self.emit_timer}


def contiguous_filled_array(array, shape):
    out_shape = tuple(map(max, zip(array.shape, shape)))
    if out_shape != array.shape:
        temp = np.full(out_shape, np.nan)
        temp[tuple(slice(0, s) for s in array.shape)] = array
        array = temp
    return np.ascontiguousarray(array)


class StorePipeline:
    def __init__(self, varnames, key_pattern, index_offset, dtype, compressor, store,
                 vertical_chunk_size=None, time_chunk_size=1):
        import fsspec

        self.key_pattern = key_pattern
        self.index_offset = index_offset
        self.vertical_chunk_size = vertical_chunk_size
        self.time_chunk_size = time_chunk_size
        self.dtype = dtype
        self.compressor = compressor
        self.store = fsspec.get_mapper(store, auto_mkdir=True)

        self.stackers = {varname: Stacker(self.time_chunk_size, self._mk_flush_fn(varname))
                         for varname in varnames}

        self.compress_timer = Timer()
        self.store_timer = Timer()

    def _mk_flush_fn(self, varname):
        def flush(i, data):
            buffer = np.stack(data, axis=0)
            vsize = buffer.shape[1]
            vstep = self.vertical_chunk_size or vsize

            with self.compress_timer:
                compressed_blocks = [self.compressor.encode(
                                        contiguous_filled_array(buffer[:, ofs : ofs + vstep],
                                                                (self.time_chunk_size, vstep, -1)))
                                     for ofs in range(0, vsize, vstep)]

            items_to_store = {
                self.key_pattern.format(var=varname, i=self.index_offset+i, v=v): block
                for v, block in enumerate(compressed_blocks)
            }

            with self.store_timer:
                self.store.setitems(items_to_store)

        return flush

    def handle_field(self, varname, i, timestamp, buffer):
        self.stackers[varname].push(buffer.astype(self.dtype))

    def finish(self):
        for name, stacker in self.stackers.items():
            stacker.flush()

    def get_timers(self):
        return {"store": self.store_timer, "compress": self.compress_timer}


class TimeAlignedZarrOutputPipeline:
    def __init__(self, varnames, store, region=None, flushes_per_chunk=1, output_in_thread=False):
        self.store = store
        ds = xr.open_dataset(store, engine="zarr")
        self.zarr_group = zarr.open_consolidated(store, mode="r+")
        self.time = ds.time.values
        self.timechunk_per_variable = {
            varname: dict(zip(ds[varname].dims, ds[varname].encoding["chunks"]))["time"]
            for varname in varnames
        }
        self.dtype_per_variable = {
            varname: ds[varname].dtype
            for varname in varnames
        }
        self.state = {
            varname: {
                "buffer": [],
                "next_index": None,
            }
            for varname in varnames
        }

        self.region = region or ()
        self.flushes_per_chunk = flushes_per_chunk
        self.output_in_thread = output_in_thread

        self.compress_timer = Timer()
        self.store_timer = Timer()


        if self.output_in_thread:
            self._write_queue = queue.Queue()
            self._write_thread = threading.Thread(target=self._worker)
            self._write_thread.start()

    def position_buffer(self, varname, timestamp):
        varstate = self.state[varname]
        if varstate["next_index"] is None:
            i = np.searchsorted(self.time, timestamp)
            assert timestamp == self.time[i], f"tried to write timestep {timestamp} which is not included in dataset"
            varstate["next_index"] = i

    def write_to_zarr(self, varname, i, buffer, last=False):
        buffer = np.stack(buffer, axis=0)

        chunk = self.timechunk_per_variable[varname]
        if (len(buffer) != chunk and not last) or i % chunk != 0:
            logging.debug("writing unaligned chunk to variable %s. This might have a performance impact!", varname)

        if len(self.zarr_group[varname].shape) == 2 and len(buffer.shape) == 3:
            # 2D var packed in 3D slice
            assert buffer.shape[1] == 1, f"collection size not 1 for 2D variable {varname}"
            buffer = buffer[:,0,:]

        region = (slice(i - len(buffer), i),) + self.region
        try:
            self.zarr_group[varname][region] = buffer
        except Exception as e:
            logging.warning(f"COULD NOT WRITE CHUNK: variable {varname}, region {region}, error: {e}")

    def _worker(self):
        done_since_last_empty = []
        while True:
            if self._write_queue.empty() and done_since_last_empty:
                logging.info("Reached empty write queue. Writes during last period: %s", done_since_last_empty)
                done_since_last_empty = []
            item = self._write_queue.get()
            self._write_queue.task_done()
            if item == "DONE":
                if done_since_last_empty:
                    logging.info("Reached end of write queue. Writes during last period: %s", done_since_last_empty)
                    done_since_last_empty = []
                break
            else:
                self.write_to_zarr(*item)
                done_since_last_empty.append(item[:2])

    def flush_var(self, varname, last=False):
        logging.debug("flush var %s", varname)
        with self.store_timer:
            varstate = self.state[varname]
            i = varstate["next_index"]
            if len(varstate["buffer"]) == 0:
                return
            if self.output_in_thread:
                self._write_queue.put((varname, i, varstate["buffer"], last))
            else:
                self.write_to_zarr(varname, i, varstate["buffer"], last)
            varstate["buffer"] = []

    def handle_field(self, varname, itime, timestamp, buffer):
        logging.debug("handle zarr %s", varname)
        varstate = self.state[varname]
        if varstate["next_index"] is None:
            self.position_buffer(varname, timestamp)
        else:
            if varstate["next_index"] >= len(self.time):
                logging.warning("received data for variable '%s' out of bounds @ %s. %d >= %d. Trying to fix this, but we'll probably crash soon.", varname, timestamp, varstate["next_index"], len(self.time))
                self.flush_var(varname)
                self.position_buffer(varname, timestamp)
            elif timestamp != self.time[varstate["next_index"]]:
                logging.warning("time jump during output of '%s'. Expected %s, received %s, this might have a performance impact!", varname, self.time[varstate["next_index"]], timestamp)
                self.flush_var(varname)
                self.position_buffer(varname, timestamp)
        assert varstate["next_index"] < len(self.time), f"write position went out of bounds {varstate['next_index']} >= {len(self.time)}"
        assert timestamp == self.time[varstate["next_index"]], f"something went wrong while positioning the output buffer {timestamp} != {self.time[varstate['next_index']]}"

        varstate["buffer"].append(buffer.astype(self.dtype_per_variable[varname]))
        old_index = varstate["next_index"]
        varstate["next_index"] += 1

        chunksize = self.timechunk_per_variable[varname]
        # Reduce chunksize for flushing to get memory footprint down (if flushes_per_chunk > 1)
        # This will result in re-reading data from disk (which can be expensive!)
        flushsize = max(1, chunksize // self.flushes_per_chunk)
        if (old_index // flushsize) != (varstate["next_index"] // flushsize):
            self.flush_var(varname)

    def finish(self):
        for varname in self.state:
            self.flush_var(varname, last=True)

        if self.output_in_thread:
            self._write_queue.put("DONE")
            self._write_thread.join()

    def get_timers(self):
        return {"store": self.store_timer}
