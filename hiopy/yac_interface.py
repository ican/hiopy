import packaging.version
import logging
import heapq
from itertools import count, repeat, product

import numpy as np
import yac as yac_module
import isodate
from isodate import parse_duration

from .grids.base import xyz2lonlat

YAC_OUT_OF_BOUND = 8

yac_version = packaging.version.parse(yac_module.version())

def print_yac_config(yac):
    import sys
    print("YAC config:", file=sys.stderr)
    for comp_name in yac.component_names:
        comp_metadata = yac.get_component_metadata(comp_name)
        if comp_metadata is not None:
            print(f"{comp_name} ({comp_metadata})", file=sys.stderr)
        for grid_name in yac.get_comp_grid_names(comp_name):
            grid_metadata = yac.get_grid_metadata(grid_name)
            if grid_metadata is not None:
                print(f"{comp_name}::{grid_name} ({grid_metadata})", file=sys.stderr)

            for field_name in yac.get_field_names(comp_name, grid_name):
                field_metadata = yac.get_field_metadata(comp_name, grid_name, field_name)
                if field_metadata is not None:
                    print(f"{comp_name}::{grid_name}::{field_name} ({field_metadata})", file=sys.stderr)

                field_timestep = yac.get_field_timestep(comp_name, grid_name, field_name)
                field_collection_size = yac.get_field_collection_size(comp_name, grid_name, field_name)
                print(f"{comp_name}::{grid_name}::{field_name} timestep: {field_timestep} collection_size: {field_collection_size}", file=sys.stderr)

def make_yac_grid(gridspec, gridname):
    verts_xyz, cells = gridspec.get_geometry()
    verts_ll = np.deg2rad(xyz2lonlat(verts_xyz))
    centers_ll = np.deg2rad(gridspec.get_centers_lonlat())
    npoints = len(cells)
    verts_per_cell = cells.shape[-1]

    grid = yac_module.UnstructuredGrid(
        gridname,
        np.full(npoints, verts_per_cell, dtype=int),
        verts_ll[:, 0],
        verts_ll[:, 1],
        cells.flatten(),
    )

    if verts_per_cell > 1:
        location = yac_module.Location.CELL
    else:
        location = yac_module.Location.CORNER

    point_id = grid.def_points(location, centers_ll[:, 0], centers_ll[:, 1])
    grid.set_global_index(gridspec.global_index, location)
    return point_id, grid, npoints


def parse_datetime(dtstring):
    if dtstring.startswith("+"):
        dtstring = dtstring[1:]
    return isodate.parse_datetime(dtstring)


def get_couple_info(complex_coupling, component):
    """
    Extracts coupling parameters for own component from a complex coupling file.

    :note: this is intended to be used with YAC3.
    """
    yac_time_reductions = {
        None: yac_module.Reduction.TIME_NONE,
        "none": yac_module.Reduction.TIME_NONE,
        "accumulate": yac_module.Reduction.TIME_ACCUMULATE,
        "average": yac_module.Reduction.TIME_AVERAGE,
        "minimum": yac_module.Reduction.TIME_MINIMUM,
        "maximum": yac_module.Reduction.TIME_MAXIMUM,
    }

    def parse_interpolation_stack(interpolation_stack):
        nntypes = {
            "ARITHMETIC_AVERAGE": yac_module.NNNReductionType.AVG,
            #TODO: more types
        }

        interp_types = {
            "n-nearest_neighbor": lambda s, d: s.add_nnn(nntypes[d.get("weighted", "ARITHMETIC_AVERAGE")], d.get("n", 1), d.get("scale", 1.)),
            #TODO: more types
        }

        stack = yac_module.InterpolationStack()
        while interpolation_stack:
            cur = interpolation_stack[0]
            interpolation_stack = interpolation_stack[1:]
            interp_types[cur["method"]](stack, cur)

        return stack

    transient_ref_to_component = {}
    for _component, _component_def in complex_coupling["components"].items():
        for ref in _component_def["transient_grid_refs"]:
            transient_ref_to_component[ref] = _component

    def get_ref(ref, prefix=""):
        component = transient_ref_to_component[ref]
        tc_def = complex_coupling["components"][component]["transient_grid_refs"][ref]
        return {prefix + "comp": component,
                prefix + "grid": tc_def["grid"],
                prefix + "field": tc_def["transient"]}

    component_def = complex_coupling["components"][component]

    upstream_couples = [
        {
            **get_ref(tc["source"]["ref"], "src_"),
            **get_ref(tc["target"]["ref"], "tgt_"),
            "coupling_timestep": tc["coupling_period"]["interval"],
            "timeunit": yac_module.TimeUnit.ISO_FORMAT,
            "time_reduction": yac_time_reductions[tc["coupling_period"]["operation"]],
            "interp_stack": parse_interpolation_stack(tc["interpolation"]),
            "src_lag": tc["source"].get("timelag", 0),
            "tgt_lag": tc["target"].get("timelag", 0),
            "mapping_on_source": 1 if tc["mapping_on_source"] else 0,
        }
        for c in complex_coupling.get("couples", [])
        for tc in c.get("transient_couples", [])
        if tc["target"]["ref"] in component_def["transient_grid_refs"]
    ]

    field_timesteps = {
        get_ref(tc[side]["ref"])["field"]: tc[side]["timestep"]
        for c in complex_coupling.get("couples", [])
        for tc in c.get("transient_couples", [])
        for side in ["source", "target"]
        if get_ref(tc[side]["ref"])["comp"] == component
    }

    fields = {
        ref["transient"]: {
            "grid": ref["grid"],
            "collection_size": ref["size"],
            "timestep": field_timesteps[ref["transient"]],
            "timeunit": yac_module.TimeUnit.ISO_FORMAT,
        }
        for ref in component_def["transient_grid_refs"].values()
    }

    return {"couples": upstream_couples, "fields": fields}

class YACComponent:
    def __init__(self, componentname, xml="coupling.xml", xsd="coupling.xsd", yac=None, lag=0, ccoupling_filename="ccoupling.yaml"):
        self.name = componentname
        self.lag = lag

        if yac_version.major == 3:
            import yaml
            with open(ccoupling_filename) as cfp:
                self.ccoupling = yaml.safe_load(cfp)
            self.coupling_info = get_couple_info(self.ccoupling, self.name)
        if yac:
            self.own_yac = False
            self.yac = yac
        else:
            self.own_yac = True
            if yac_version.major == 2:
                self.yac = yac_module.YAC(xml, xsd)
            else:
                calendar = {
                    "proleptic-gregorian": yac_module.Calendar.PROLEPTIC_GREGORIAN,
                    #TODO: more calendars (?) hiopy currently might not support anything else though
                }[self.ccoupling["dates"]["calendar"]]
                yac_module.def_calendar(calendar)
                self.yac = yac_module.YAC()
                self.yac.def_datetime(self.ccoupling["dates"]["start"], self.ccoupling["dates"]["end"])

        self.comp_id = self.yac.def_comp(componentname)

    def gen_field_timestamps(self, field):
        logging.debug("YAC: getting timestamp of field %s", field.name)
        if yac_version.major == 3:
            step = isodate.parse_duration(field.timestep)
        else:
            step = isodate.parse_duration(field.model_timestep)
        start = parse_datetime(self.yac.start_datetime)
        end = parse_datetime(self.yac.end_datetime)
        current = start + (self.lag * step)
        while current <= end:
            yield current
            current += step

    def activate(self):
        if self.own_yac:
            if yac_version.major == 3:
                self.yac.enddef()
                if logging.getLogger().isEnabledFor(logging.DEBUG):
                    print_yac_config(self.yac)
            else:
                self.yac.search()

    def get_listener(self, gridname, gridspec, fieldnames):
        return YACListener(gridname, gridspec, fieldnames, self)


    def get_emitter(self, gridname, gridspec, fieldnames):
        return YACEmitter(gridname, gridspec, fieldnames, self)

    def create_field(self, name, point_id):
        if yac_version.major == 3:
            field_info = self.coupling_info["fields"][name]
            return yac_module.Field.create(name,
                                           self.comp_id,
                                           point_id,
                                           field_info["collection_size"],
                                           field_info["timestep"],
                                           field_info["timeunit"])
        else:
            return yac_module.Field.create(name, self.comp_id, point_id)



class YACListener:
    def __init__(self,
                 gridname,
                 gridspec,
                 fieldnames,
                 component,
                 buffer_entire_timestep=False,
                 ):



        self.fieldnames = fieldnames
        point_id, grid, npoints = make_yac_grid(gridspec, gridname)

        self.fields = [component.create_field(name, point_id) for name in self.fieldnames]

        self.timestamped_fields = [zip(component.gen_field_timestamps(field), repeat(i), count(0))
                                   for i, field in enumerate(self.fields)]
        self.buffer_entire_timestep = buffer_entire_timestep

        if yac_version.major == 3:
            for couple in component.coupling_info["couples"]:
                logging.debug("defining couple %s", couple)
                component.yac.def_couple(**{**couple,
                                            "time_reduction": couple["time_reduction"].value  # fix for older YAC versions, may be removed later
                                            })

    def __iter__(self):
        last_ts = None
        acc = []

        def flush():
            for item in acc:
                logging.debug("process %s @ %s", item[0], item[2])
                yield item
            acc.clear()

        for timestamp, ifield, itime in heapq.merge(*self.timestamped_fields):
            if timestamp != last_ts and last_ts is not None:
                logging.info("YAC listener: timestep %s received completely", last_ts)

            if not self.buffer_entire_timestep or timestamp != last_ts:
                yield from flush()

            if timestamp != last_ts:
                logging.info("YAC listener: ready for timestep %s", timestamp)

            last_ts = timestamp

            buf, info = self.fields[ifield].get()
            logging.debug("recv %s @ %s", self.fieldnames[ifield], timestamp)
            if info == YAC_OUT_OF_BOUND:
                raise RuntimeError(f"tried to access out of bound field '{self.fieldnames[ifield]}' @ {timestamp} from yac")

            acc.append((self.fieldnames[ifield], itime, np.datetime64(timestamp), buf))

        logging.info("YAC listener: timestep %s received completely", last_ts)
        yield from flush()
        logging.info("YAC listener: done")



class YACEmitter:
    def __init__(self,
                 gridname,
                 gridspec,
                 fieldnames,
                 component,
                 ):

        self.fieldnames = fieldnames
        point_id, grid, npoints = make_yac_grid(gridspec, gridname)
        self.fields = {name: component.create_field(name, point_id) for name in self.fieldnames}

    def emit(self, fieldname, itime, timestamp, buffer):
        self.fields[fieldname].put(buffer)
