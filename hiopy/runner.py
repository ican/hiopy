import logging
from .timer import Timer
from .aggregation import NoAggregation
import resource


def run_pipelines(pipelines, component, listener, vartable=None, aggregation=None, is_initial_aggregation_step=False):
    search_timer = Timer()
    mainloop_timer = Timer()
    aggregation = aggregation or NoAggregation()

    try:
        with search_timer:
            component.activate()

        with mainloop_timer:
            logging.debug("memdebug: rss limit (soft: %d, hard: %d)", *resource.getrlimit(resource.RLIMIT_RSS))
            logging.debug("memdebug: start maxrss: %d)", resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)
            for fieldname, i, timestamp, buffer in listener:
                varname = fieldname if vartable is None else vartable[fieldname]

                if is_initial_aggregation_step:
                    buffer = aggregation.prepare(buffer)

                for pipeline in pipelines:
                    if getattr(pipeline, "requires_raw_agg", False):
                        pipeline.handle_field(varname, i, timestamp, buffer)
                    else:
                        pipeline.handle_field(varname, i, timestamp, aggregation.finalize(buffer))
                logging.debug("pipeline %s done", varname)

                logging.debug("memdebug: after processing %s, maxrss: %d", varname, resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)

        for pipeline in pipelines:
            pipeline.finish()

    finally:
        timers = [
            ("search", search_timer),
            ("mainloop", mainloop_timer)
        ]
        for pipeline in pipelines:
            for name, timer in pipeline.get_timers().items():
                timers.append((name, timer))

        for name, timer in timers:
            print(
                f"component {component.name}: {name} time: {timer.time_spent} s, {timer.invocations} invocations"
            )
        print(flush=True)
