class Stacker:
    def __init__(self, size, flush_fn):
        assert size >= 1
        self.size = size
        self.flush_fn = flush_fn
        self.stack = []
        self.flushcounter = 0

    def push(self, data):
        self.stack.append(data)
        if len(self.stack) == self.size:
            self.flush()

    def flush(self):
        if len(self.stack) == 0:
            return
        self.flush_fn(self.flushcounter, self.stack)
        self.stack = []
        self.flushcounter += 1
