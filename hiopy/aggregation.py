import numpy as np

class NoAggregation:
    buffer_stretch_factor = 1

    def prepare(self, buffer):
        return buffer

    def aggregate(self, buffer):
        return buffer

    def finalize(self, buffer):
        return buffer


class TrivialAssociativeAggregation:
    buffer_stretch_factor = 1

    def __init__(self, nagg=4):
        self.nagg = nagg

    def prepare(self, buffer):
        return buffer

    def aggregate(self, buffer):
        return self._op(buffer.reshape(buffer.shape[:-1] + (buffer.shape[-1] // self.nagg, self.nagg)), axis=-1)

    def finalize(self, buffer):
        return buffer


class SumAggregation(TrivialAssociativeAggregation):
    _op = staticmethod(np.sum)


class NanSumAggregation(TrivialAssociativeAggregation):
    _op = staticmethod(np.nansum)


class MeanAggregation(TrivialAssociativeAggregation):
    _op = staticmethod(np.mean)


class NanMeanAggregation:
    buffer_stretch_factor = 2

    def __init__(self, nagg=4):
        self.nagg = 4

    def prepare(self, buffer):
        count = (~np.isnan(buffer)).astype(buffer.dtype)
        return np.concatenate([buffer, count], axis=0)

    def aggregate(self, buffer):
        return np.nansum(buffer.reshape(buffer.shape[:-1] + (buffer.shape[-1] // self.nagg, self.nagg)), axis=-1)

    def finalize(self, buffer):
        n = len(buffer) // 2
        assert 2 * n == len(buffer)
        sumdata = buffer[:n]
        countdata = buffer[n:]
        with np.errstate(divide='ignore', invalid='ignore'):
            # we might get 0 / 0 divisions and it's ok (we want nan as a result)
            return sumdata / countdata

_aggregations = {
    "no": NoAggregation,
    "sum": SumAggregation,
    "nansum": NanSumAggregation,
    "mean": MeanAggregation,
    "nanmean": NanMeanAggregation,
}

def get_aggregation(name):
    return _aggregations[name]
