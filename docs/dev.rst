Development
===========

Development of ``hiopy`` is happening on `GWDG Gitlab <https://gitlab.gwdg.de/ican/hiopy>`_.
Please submit issues or merge requests :-)


some concepts behind hiopy
--------------------------

.. _no-parallel-write:

no parallel writes to a single file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Parallel writes to single files always require some amount of coordination, which makes things more complicated and potentially blocking.
The concept of a dataset can span many files, thus often it's not necessary to write to single files in parallel.

single threaded processes
~~~~~~~~~~~~~~~~~~~~~~~~~
``hiopy`` works on single threaded processes.
To achieve parallelism, many processes are started.
This simplifies development and eases parallelism across many nodes inside a compute cluster.

no metadata output in parallel processes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Output metadata (e.g. structural information, attributes...) usually spans across multiple data chunks.
It's likely that some of those data chunks should be written in parallel.
Thus, handling metadata in chunk writing processes would likely require parallel access to single metadata files.
We don't want that (see :ref:`no-parallel-write`).
Instead, metadata should be written by separate (non-parallel) processes, which are only responsible for writing metadata.

some internal nomenclature
--------------------------

The current names used here should probably be changed, but the aren't any better names yet.
If you find better ones, please make a suggestion!

component
~~~~~~~~~
A ``component`` in ``hiopy`` is responsible for handling inbound and outbound network traffic of data chunks. An instance of ``YAC`` with a registered YAC-component could form a ``hiopy`` ``component``. Others may follow.
The current use of the ``component`` abstraction is to facilitate unit tests without depending on a parallel YAC setup.

pipeline
~~~~~~~~
A ``pipeline`` is a bundled set of instructions of what to do when new data needs to be processed. Examples would be:

* coarsening and re-publishing data
* collecting and storing data
