Coupling Formats
================

This page (should) describe various formats for coupling configurations. The specifics might change, in particular because upcoming YAC versions will change their way of defining coupling configurations.

``hiopy`` has some coupling configuration formats which help setting up complex coupling schemes, which might be required for a :ref:`multilevel-setup`.

.. _simple-coupling-format:

simple format
-------------

complex format
--------------

XML format
----------
