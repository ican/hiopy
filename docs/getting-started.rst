.. _getting-started:

Getting Started
===============

The key idea of ``hiopy`` is to leverage the `YAC <https://dkrz-sw.gitlab-pages.dkrz.de/yac/index.html>`_ coupler to redistribute data from model compute nodes to **independent** chunk output nodes.
This setup reduces communication between processes and simplifies scaling up to finer resolutions, but requires some planning beforehand.


.. diagrams::

   from diagrams import Cluster, Edge
   from diagrams.custom import Custom
   from sphinx_diagrams import SphinxDiagram
   from diagrams.onprem.compute import Server
   from diagrams.generic.storage import Storage

   with SphinxDiagram(title="", direction="LR"):
       with Cluster("model"):
           icons = [Server() for _ in range(5)]

       with Cluster("hiopy"):
           hiopys = [Server(f"output {i}") for i in range(2)]

       zarr = Custom("Zarr", "../../../images/zarr.png")

       for icona, iconb in zip(icons[:-1], icons[1:]):
           icona >> Edge() << iconb

       for icon in icons[:3]:
           icon >> hiopys[0]

       for icon in icons[2:]:
           icon >> hiopys[1]

       for hiopy in hiopys:
           hiopy >> zarr


Coupling setup
--------------

.. highlight:: yaml

``hiopy`` can act as one (or multiple) YAC component in a coupled setup. In the scenario shown above, the two output processes would form one YAC component, which is coupled to the model component. Using the :ref:`simple-coupling-format`, the corresponding coupling configuration might look like this::

   components:
     model:
       model: model name
       simulated: simulation description
       grid: model_grid
       time:
         step: PT1S
         lag: 0
       have:
         - var1
         - var2
     hiopy:
       grid: output_grid
       time:
         step: PT1S
         lag: 0
       want:
         - var1
         - var2
   dates:
     start: "2020-01-01T00:00:00.000"
     end: "2020-01-01T00:00:10.000"
     calendar: "proleptic-gregorian"
   interpolations:
     default:
       space:
         - method: n-nearest_neighbor
           n: 1
           weighted: ARITHMETIC_AVERAGE
       time: average


The coupling configuration can be translated to YAC XML format using ``hiopy s2x`` on the command line, input is either ``stdin`` or from a file using ``-i``, output is either ``stdout`` or written to a file using ``-o`` option.

Running model and output
------------------------

``hiopy`` output workers must be started concurrently using a common MPI configuration. This can either be done using ``mpirun`` or ``slurm`` etc...

The particular configuration of an output worker will depend on the output grid and format.
Currently, there's healpix output implemented in: ``hiopy healpix_output``. For now, please have a look at ``hiopy healpix_output -h`` to get some information about available options:

.. program-output:: hiopy healpix_output -h


.. _multilevel-setup:

Multilevel Setup
----------------

If higher resolution model setups are used, it might be desirable to write output in multiple resolutions. In order to do this with a scalable communication pattern, ``hiopy`` output workers can re-publish coarser versions of the output data to be fed back into another set of output worker, essentially creating a hierarchy of output workers:

.. diagrams::

   from diagrams import Cluster, Edge
   from diagrams.custom import Custom
   from sphinx_diagrams import SphinxDiagram
   from diagrams.onprem.compute import Server
   from diagrams.generic.storage import Storage

   with SphinxDiagram(title="", direction="LR"):
       with Cluster("model"):
           icons = [Server() for _ in range(10)]

       with Cluster("hiopy1"):
           hiopys1 = [Server(f"output {i}") for i in range(4)]

       with Cluster("hiopy2"):
           hiopy2 = Server(f"output 4")

       zarr1 = Custom("Zarr fine", "../../../images/zarr.png")
       zarr2 = Custom("Zarr coarse", "../../../images/zarr.png")

       for icona, iconb in zip(icons[:-1], icons[1:]):
           icona >> Edge() << iconb

       for icon in icons[:3]:
           icon >> hiopys1[0]

       for icon in icons[2:5]:
           icon >> hiopys1[1]

       for icon in icons[5:8]:
           icon >> hiopys1[2]

       for icon in icons[7:10]:
           icon >> hiopys1[3]

       for hiopy1 in hiopys1:
           hiopy1 >> hiopy2
           hiopy1 >> zarr1

       hiopy2 >> zarr2

