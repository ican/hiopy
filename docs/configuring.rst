.. _configuring:

Configuring hiopy
=================

Depending on your setup, the configuration of ``hiopy`` may be non-trivial as a bunch of communication has to be set up and some sequencing of parts has to be done by the user. This page should provide an overview on what's needed.

Running with ICON
-----------------

The way of how to best run ``hiopy`` is still changing, so this section might have to be updated from time to time...

In order to use ``hiopy`` as a YAC-coupled output component, we'll have to:

* initialize Zarr stores
* create a coupling configuration for YAC
* figure out which ``hiopy`` processes have to be run concurrently with the model, and run them

These three steps can be executed independently (to some extent they have to be run independent, because the steps have to be done at different point in the model execution workflow), but they need to share some common information about the to-be-created datasets. That's where the dataset configuration plan (``.plan.yaml``) helps: it's a common information base for the required steps. Currently the dataset configuration plan is provided by the `icon_variables <https://gitlab.gwdg.de/ican/icon_variables>`_ repository.


overview diagram
~~~~~~~~~~~~~~~~

.. diagrams::

   from diagrams import Cluster, Edge
   from diagrams.custom import Custom
   from diagrams.programming.flowchart import Database, Document, MultipleDocuments, Merge, Action
   from sphinx_diagrams import SphinxDiagram
   from diagrams.onprem.compute import Server
   from diagrams.generic.storage import Storage

   with SphinxDiagram(title="", direction="LR"):


       with Cluster("icon_variables"):
           vartable = Database("variables.yaml")
           dsconfig = Database("configurations/....yaml")
           dsconfig >> Action("render") >> Document("....html")
           plan = Document("....plan.yaml")
           [vartable, dsconfig] >> Action("expand_plan") >> plan
           icon_havelist = Document("icon havelist")
           vartable >> Action("havelist") >> icon_havelist

       with Cluster("mkexp / hiopy"):
           icon_coupling = Database("icon coupling")

           extend_time = Action("configure extend_time")
           stores = Custom("Zarr stores", "../../../images/zarr.png")
           process_list = Document("icon_mpmd.conf")
           hiopy_coupling = Document("hiopy coupling")
           coupling_yaml = Document("coupling.yaml")
           ccoupling_yaml = Document("ccoupling.yaml")
           coupling_xml = Document("coupling.xml")

           icon_coupling >> Edge(label="classic way", style="dashed") >> coupling_xml

           plan >> Action("configure init_zarr") >> stores
           plan >> extend_time

           plan >> Action("configure processes") >> process_list
           plan >> Action("configure coupling") >> hiopy_coupling
           [hiopy_coupling, icon_coupling, icon_havelist >> Action("fill_placeholder")] >> Action("merge_couplings") >> Action("filter_unwanted_coupling") >> Action("multilevel_coupling") >> coupling_yaml
           coupling_yaml >> Action("s2c") >> ccoupling_yaml
           ccoupling_yaml >> Action("c2x") >> Edge(label="YAC2 only") >> coupling_xml

           extend_time >> stores >> extend_time

           workers = Action("*_output (workers)")
           icon = Custom("ICON", "../../../images/r2b02_europe.png")
           yac = Custom("YAC", "../../../images/yak_small.jpg")
           [process_list, yac] >> workers >> stores
           icon >> yac
           coupling_xml >> Edge(label="YAC2 only") >> yac
           ccoupling_yaml >> Edge(label="YAC3 only") >> workers


initializing Zarr stores
~~~~~~~~~~~~~~~~~~~~~~~~

Once, before and data is written to a zarr store, the store must be initialized. This initialization will lay out all the expected variables, the dimensions, chunking and other metadata (generally, all the ``.zgroup``, ``.zarray`` and ``.zattrs``). This step is important for two reasons: write-access to metadata must be synchronized across all writes which is much simpler if done in advance and subsequent writes to the data chunks can use the metadata information for configuration (e.g. compression, chunk, data type etc...). As the model might be run a few time steps at a time and the total duration could potentially be extended after the initial run, it's useful to be able to extend the ``time`` dimension even after some data has been written. For this reason, we've split the initialization of the zarr stores into two steps:

* ``init_zarr`` creates all metadata but initializes the size of the ``time`` dimension to ``0``. This must be run *excactly once* before the model is started.
* ``extend_time`` extends the time dimension to grow the dataset. This must be run before the model is started to define all time steps on which output is expected. ``extend_time`` can (and should) be repeated whenever the model run is extended (e.g. prior to each slurm job).

There are two modes to run the initialization commands. If called directly as ``hiopy init_zarr`` and ``hiopy extend_time``, the user must supply a dataset definition and time ranges and periods. It's also possible to call the initialization via ``hiopy configure init_zarr`` and ``hiopy configure extend_time``. When using the latter variant, the user must supply a dataset configuration plan (``.plan.yaml``) which holds most of the required information. Using ``hiopy configure`` will also automatically loop over all datasets defined in the plan.


create a coupling configuration for YAC
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``hiopy`` relies heavily on YAC and thus, creating the coupling configuration is and important step. The current approach is to define three partial coupling configurations, which need to be merged to build the final configuration:

* the icon coupling: that's the classical coupling definition (e.g. used to couple atmosphere and ocean components) which previously was the only coupling definition
* the icon havelist: that's just the ``have:`` part of a coupling configuration and should list all the variables which could be produced by ICON (no matter if they actually are used)
* the hiopy coupling: that's what hiopy ``want:`` s from the model (i.e. all the output variables)

The three parts are presented in hiopy simple coupling yaml format. The icon coupling is expected to be provided by the model workflow system (e.g. ``mkexp``). The icon havelist should eventually come from ICON itself, but for now can be obtained from the ``icon_variables`` repo: https://ican.pages.gwdg.de/icon_variables/havelist_template.yaml. The ``havelist_template`` contains placeholders for the sizes of the available vertical dimensions which have to be filled either by ``mkexp`` or ``hiopy fill_placeholder`` to provide a valid partial coupling definition. The hiopy coupling can be obtained using ``hiopy configure coupling`` from the dataset configuration plan or must be provided by the user.

The three parts are then merged using ``hiopy merge_couplings`` and excessive ``have:`` entries can then be stripped using ``hiopy filter_unwanted_coupling``. For now (eventually this might go into the hiopy coupling), users have to run ``hiopy multilevel_coupling`` at this stage to generate all ``hiopy``-internal coupling configuration. The coupling configuration now has to be translated into ``hiopy``-complex-configuration format using ``hiopy s2c``. The remaining steps differ between YAC versions:

**YAC2:**

The final coupling can than be transformed to YAC2-XML using ``hiopy c2x``. There's also a shortcut ``hiopy s2x`` which directly transforms simple configuration to xml.

**YAC3:**

Hiopy uses YAC3 runtime configuration to configure the coupling. However, to do so, the worker needs access to the (complex) configuration file created by ``hiopy s2c``. By default, the file should be named ``ccoupling.yaml``.

which processes have to run
~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of required ``hiopy`` worker processes including command line arguments can be obtained from the dataset configuration plan using ``hiopy configure processes``. This list can be used to generate a SLURM mpmd configuration.
