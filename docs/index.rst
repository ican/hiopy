.. hiopy documentation master file, created by
   sphinx-quickstart on Thu Feb 23 00:32:14 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hiopy's documentation!
=================================

``hiopy`` writes model output in chunks, parallel.

In a typical setup, ``hiopy`` is coupled to the model (e.g. `ICON <https://code.mpimet.mpg.de/projects/iconpublic>`_) using `YAC <https://dkrz-sw.gitlab-pages.dkrz.de/yac/index.html>`_
and output is written in `zarr <https://zarr.readthedocs.io/>`_ to any `fsspec <https://filesystem-spec.readthedocs.io>`_ supported storage.

.. diagrams::

   from diagrams import Cluster
   from diagrams.custom import Custom
   from sphinx_diagrams import SphinxDiagram
   from diagrams.onprem.compute import Server
   from diagrams.generic.storage import Storage

   with SphinxDiagram(title=""):
       yac = Custom("YAC", "../../../images/yak_small.jpg")
       icon = Custom("Model", "../../../images/r2b02_europe.png")
       zarr = Custom("Zarr", "../../../images/zarr.png")
       hiopy = Server("hiopy")
       icon >> yac >> hiopy >> zarr

In order to write the output parallelized, multiple ``hiopy`` processes run concurrently in a coordinated manner.
Each process is associated with a fixed set output chunks.

Installation
------------

``hiopy`` can be installed using ``pip``::

   pip install git+https://gitlab.gwdg.de/ican/hiopy.git

In order to use it with `YAC <https://dkrz-sw.gitlab-pages.dkrz.de/yac/index.html>`_, YAC must also be installed (including Python-bindings).


Running
-------

``hiopy`` comes with a single executable and has several subcommands. Please use::

   hiopy -h

to get a list of currently installed subcommands and use::

   hiopy <subcommand> -h

to get help about the subcommand.
For further details, please refer to :ref:`getting-started`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   getting-started
   configuring
   coupling-formats
   resources
   dev
