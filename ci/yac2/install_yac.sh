#!/bin/bash
set -e

# installs yac with python bindings and depencencies in an ubuntu environment

SOURCE_DIR="/src"
YAC_VERSION="2.6.2"
YAXT_VERSION="0.9.3.1"

apt-get install -y git autoconf build-essential gfortran wget \
    libmpich-dev libopenmpi-dev libxml2-dev \
    python3 python3-pip python3-numpy cython3

# !! this is only meant to be used in CI, where the scripts might be executed as root
export OMPI_ALLOW_RUN_AS_ROOT=1
export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1

cd $SOURCE_DIR

if [ ! -d "yaxt-$YAXT_VERSION" ]; then
    wget "https://swprojects.dkrz.de/redmine/attachments/download/523/yaxt-$YAXT_VERSION.tar.gz"
    tar -xvf yaxt-$YAXT_VERSION.tar.gz 
fi
pushd yaxt-$YAXT_VERSION
./configure --prefix=/usr
make
make install
popd


if [ ! -d yac ]; then
    git clone https://gitlab.dkrz.de/dkrz-sw/yac.git
else
    git fetch
fi
pushd yac
git checkout release-$YAC_VERSION
autoreconf 
CC=mpicc FC=mpif90 CFLAGS=-fPIC XML2_CFLAGS=-I/usr/include/libxml2/ ./configure --enable-python-bindings --prefix=/usr
make
make install
python3 -m pip install ./python/
