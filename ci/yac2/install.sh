#!/bin/bash
set -e

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
${__dir}/install_yac.sh
