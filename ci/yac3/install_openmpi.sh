#!/bin/bash
set -e

# to be on the save side, we compile openmpi ourselves by default, this variable can override this
if [ -z "${USE_SYSTEM_MPI}" ]; then
    pushd /src
    if [ ! -d "openmpi-4.1.5" ]; then
        wget "https://download.open-mpi.org/release/open-mpi/v4.1/openmpi-4.1.5.tar.bz2"
        tar -xvf openmpi-4.1.5.tar.bz2
    fi
    cd openmpi-4.1.5
    ./configure --prefix=/usr --enable-mca-no-build=op-avx  # AVX extensions seem to be broken (at least on an OSX host)
    make
    make install
    popd
else
    apt-get install -y mpi-default-dev
fi
