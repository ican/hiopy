#!/bin/bash
set -e

SOURCE_DIR="/src"
YAXT_VERSION="0.9.3.1"

cd $SOURCE_DIR

if [ ! -d "yaxt-$YAXT_VERSION" ]; then
    wget "https://swprojects.dkrz.de/redmine/attachments/download/523/yaxt-$YAXT_VERSION.tar.gz"
    tar -xvf yaxt-$YAXT_VERSION.tar.gz
fi
pushd yaxt-$YAXT_VERSION
if [ ! -f "config.status" ]; then
    ./configure --prefix=/usr
fi
make
make install
popd
