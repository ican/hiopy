#!/bin/bash
set -e

# installs yac with python bindings and depencencies in an ubuntu environment

SOURCE_DIR="/src"
#YAC_VERSION="3.0.2_p1"
YAC_VERSION="3.0.1_p1"

pip install numpy cython

cd $SOURCE_DIR

if [ ! -d yac ]; then
    git clone https://gitlab.dkrz.de/dkrz-sw/yac.git
else
    git fetch
fi
pushd yac
if [ ! -f "config.status" ]; then
    git checkout release-$YAC_VERSION
    autoreconf
    CC=mpicc FC=mpif90 CFLAGS=-fPIC ./configure --enable-python-bindings --prefix=/usr MPI_LAUNCH="mpirun --oversubscribe"
fi
make
make check
make install
python3 -m pip install ./python/
popd
