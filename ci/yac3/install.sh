#!/bin/bash
set -e

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
${__dir}/install_deps.sh
python -m venv /opt/venv
. /opt/venv/bin/activate
${__dir}/install_openmpi.sh
${__dir}/install_yaxt.sh
${__dir}/install_fyaml.sh
${__dir}/install_yac.sh
